package com.fii.appointment_service.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fii.appointment_service.dtos.MedicationDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
@RunWith(SpringRunner.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class MedicationControllerIT {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper mapper;

    @Test
    public void getMedications_shouldReturnOk() throws Exception {
        this.mockMvc.perform(get("/api/v1/medications"))
                .andExpect(status().isOk());
    }

    @Test
    public void getMedicationWithId_1_shouldReturnOk_whenMedicationWithId_1_isInDatabase() throws Exception {
        Long medicationId = 1L;

        this.mockMvc.perform(get("/api/v1/medications/{id}", medicationId))
                .andExpect(status().isOk());
    }

    @Test
    public void getMedicationWithId_100_shouldReturnNotFound_whenMedicationWithId_100_isNotInDatabase() throws Exception {
        Long medicationId = 100L;

        this.mockMvc.perform(get("/api/v1/medications/{id}", medicationId))
                .andExpect(status().isNotFound());
    }

    @Test
    public void createMedication_shouldReturnCreated() throws Exception {
        MedicationDto medication =
                new MedicationDto("Medication name", "Medication brand");

        this.mockMvc.perform(post("/api/v1/medications")
                .content(mapper.writeValueAsString(medication))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    @Test
    public void deleteMedicationWithId_1_shouldReturnNoContent_whenMedicationWithId_1_isInDatabase() throws Exception {
        Long medicationId = 1L;

        this.mockMvc.perform(delete("/api/v1/medications/{id}", medicationId))
                .andExpect(status().isNoContent());
    }

    @Test
    public void deleteMedicationWithId_100_shouldReturnNotFound_whenMedicationWithId_100_isNotInDatabase() throws Exception {
        Long medicationId = 100L;

        this.mockMvc.perform(delete("/api/v1/medications/{id}", medicationId))
                .andExpect(status().isNotFound());
    }
}
