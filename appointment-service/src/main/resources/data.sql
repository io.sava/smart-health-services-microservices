-- hospitals
insert into hospitals(name, address)
values ('Spitalul Clinic Judetean de Urgenta Sf. Spiridon Iasi', 'Bd. Independenței nr. 1, cod 700111, IAȘI');
insert into hospitals(name, address)
values ('Spitalul Clinic de Boli Infectioase Sfanta Parascheva Iasi', 'Str. Octav Botez nr. 2, Iasi, Iasi');
insert into hospitals(name, address)
values ('Spitalul Clinic de Urgenta Militar Dr. Iacob Czihac Iasi', 'Str. Berthelot Henri Mathias nr. 7-9, Iasi, Iasi');

-- departments
insert into departments(hospital_id)
values (1);
insert into departments(hospital_id)
values (1);
insert into departments(hospital_id)
values (1);
insert into departments(hospital_id)
values (1);
insert into departments(hospital_id)
values (1);
insert into departments(hospital_id)
values (1);

insert into departments(hospital_id)
values (2);
insert into departments(hospital_id)
values (2);
insert into departments(hospital_id)
values (2);
insert into departments(hospital_id)
values (2);
insert into departments(hospital_id)
values (2);
insert into departments(hospital_id)
values (2);

insert into departments(hospital_id)
values (3);
insert into departments(hospital_id)
values (3);
insert into departments(hospital_id)
values (3);
insert into departments(hospital_id)
values (3);
insert into departments(hospital_id)
values (3);
insert into departments(hospital_id)
values (3);

-- patients
insert into patients(cnp, name, age, address)
values ('1990731336389', 'Sava Ioan', 21, 'Falticeni, Suceava');
insert into patients(cnp, name, age, address)
values ('1990731336388', 'Sava Ionescu', 21, 'Falticeni, Suceava');

-- nurses
insert into nurses(name, department_id)
values ('Angela Cornea', 1);
insert into nurses(name, department_id)
values ('Ionela Gheorghe', 3);
insert into nurses(name, department_id)
values ('Miruna Vulpes', 7);
insert into nurses(name, department_id)
values ('Felicia Bogza', 10);
insert into nurses(name, department_id)
values ('Loredana Grigorescu', 13);
insert into nurses(name, department_id)
values ('Veronica Vasile', 14);
insert into nurses(name, department_id)
values ('Celestina Tugurlan', 16);

-- doctors
insert into doctors(name, department_id)
values ('Virgil Laurentiu', 1);
insert into doctors(name, department_id)
values ('Mirel Titirez', 2);
insert into doctors(name, department_id)
values ('Emanuel Gheorghe', 3);
insert into doctors(name, department_id)
values ('Arsenie Romanescu', 2);
insert into doctors(name, department_id)
values ('Iulien Grigorescu', 1);
insert into doctors(name, department_id)
values ('Nicusor Luca', 4);
insert into doctors(name, department_id)
values ('Calin Balcescu', 5);
insert into doctors(name, department_id)
values ('Miron Stancu', 3);
insert into doctors(name, department_id)
values ('Jan Muresan', 6);

insert into doctors(name, department_id)
values ('Luca Dumitru', 7);
insert into doctors(name, department_id)
values ('Dorin Galca', 7);
insert into doctors(name, department_id)
values ('Gabriel Enache', 8);
insert into doctors(name, department_id)
values ('Paul Puscas', 9);
insert into doctors(name, department_id)
values ('Bogdan Andreescu', 8);
insert into doctors(name, department_id)
values ('Matei Barbu', 7);
insert into doctors(name, department_id)
values ('Toma Zamfir', 10);
insert into doctors(name, department_id)
values ('Sebastian Raceanu', 11);
insert into doctors(name, department_id)
values ('Teodosie Ilionescu', 9);
insert into doctors(name, department_id)
values ('Raul Plesu', 12);

insert into doctors(name, department_id)
values ('Sergiu Gheorghiu', 13);
insert into doctors(name, department_id)
values ('Cezar Lupul', 13);
insert into doctors(name, department_id)
values ('Valerian Roman', 14);
insert into doctors(name, department_id)
values ('Jean Negrescu', 15);
insert into doctors(name, department_id)
values ('Ion Pecurar', 14);
insert into doctors(name, department_id)
values ('Serghei Alexandrescu', 13);
insert into doctors(name, department_id)
values ('Geza Niculescu', 16);
insert into doctors(name, department_id)
values ('Tudor Serban', 17);
insert into doctors(name, department_id)
values ('Silviu Arcos', 15);
insert into doctors(name, department_id)
values ('Costel Avramescu', 18);

-- medications
insert into medications(name, brand)
values ('Ibuprofen', 'Cipla');
insert into medications(name, brand)
values ('FLUOROURACIL', 'EBEWE');
insert into medications(name, brand)
values ('ABACAVIR', 'TERAPIA');
insert into medications(name, brand)
values ('ABASAGLAR', 'LILLY FRANCE');
insert into medications(name, brand)
values ('ALOPURINOL AUROBINDO', 'AUROBINDO PHARMA ROMANIA');
insert into medications(name, brand)
values ('SIMVASTATIN AUROBINDO', 'AUROBINDO PHARMA ROMANIA');
insert into medications(name, brand)
values ('OMEPRAZOL TERAPIA', 'TERAPIA SA');
insert into medications(name, brand)
values ('TICLODIN', 'AC HELCOR SRL');
insert into medications(name, brand)
values ('CARVEDILOL SANDOZ', 'HEXAL AG');
insert into medications(name, brand)
values ('CORYOL', 'KRKA D.D.');

-- appointments
insert into appointments(date, diagnosis, doctor_id, nurse_id, patient_id)
values ('2020-08-19', 'Sanatos', 1, 1, 1);
insert into appointments(date, diagnosis, doctor_id, nurse_id, patient_id)
values ('2020-09-19', '-', 2, 2, 1);
insert into appointments(date, diagnosis, doctor_id, nurse_id, patient_id)
values ('2020-08-19', '-', 1, 3, 2);
insert into appointments(date, diagnosis, doctor_id, nurse_id, patient_id)
values ('2020-09-19', '-', 3, 4, 2);

-- prescriptions
insert into prescriptions(dose, appointment_id, medication_id)
values ('25 mg', 1, 1);
insert into prescriptions(dose, appointment_id, medication_id)
values ('100 mg', 1, 2);
insert into prescriptions(dose, appointment_id, medication_id)
values ('75 mg', 3, 3);
insert into prescriptions(dose, appointment_id, medication_id)
values ('20 mg', 3, 4);
insert into prescriptions(dose, appointment_id, medication_id)
values ('75 mg', 4, 3);
insert into prescriptions(dose, appointment_id, medication_id)
values ('20 mg', 4, 5);
insert into prescriptions(dose, appointment_id, medication_id)
values ('20 mg', 4, 6);