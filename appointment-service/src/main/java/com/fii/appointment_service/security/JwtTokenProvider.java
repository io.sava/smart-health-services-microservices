package com.fii.appointment_service.security;

import com.fii.appointment_service.entities.Role;
import com.fii.appointment_service.entities.User;
import com.fii.appointment_service.exceptions.CustomException;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.util.Base64;
import java.util.LinkedHashMap;
import java.util.Optional;

@Component
public class JwtTokenProvider {
    private static final String HEADER_PREFIX = "Bearer ";
    private static final String AUTH_FIELD = "auth";
    private static final String AUTHORITY_FIELD = "authority";
    private static final String OWNER_ID_FIELD = "ownerId";

    @Value("${security.jwt.token.secret-key}")
    private String secretKey;

    @PostConstruct
    protected void init() {
        secretKey = Base64.getEncoder().encodeToString(secretKey.getBytes());
    }

    public Authentication getAuthentication(String token) {
        User user = new User();
        user.setEmail(getEmail(token));
        user.setRole(getRole(token));
        user.setOwnerId(getOwnerId(token));
        return new UsernamePasswordAuthenticationToken(user, "", user.getAuthorities());
    }

    public String getEmail(String token) {
        return Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody().getSubject();
    }

    private Role getRole(String token) {
        return Role.valueOf((String) ((LinkedHashMap)
                Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody().get(AUTH_FIELD)).get(AUTHORITY_FIELD));
    }

    private Long getOwnerId(String token) {
        Optional<Object> ownerId = Optional.ofNullable(Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody().get(OWNER_ID_FIELD));
        if (ownerId.isPresent()) {
            return Long.parseLong(ownerId.get().toString());
        }
        return null;
    }

    public String resolveToken(HttpServletRequest request) {
        String bearerToken = request.getHeader(HttpHeaders.AUTHORIZATION);
        if (bearerToken != null && bearerToken.startsWith(HEADER_PREFIX)) {
            return bearerToken.substring(7);
        }
        return null;
    }

    public boolean validateToken(String token) {
        try {
            Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token);
            return true;
        } catch (JwtException | IllegalArgumentException exception) {
            throw new CustomException("Expired or invalid JWT token", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}