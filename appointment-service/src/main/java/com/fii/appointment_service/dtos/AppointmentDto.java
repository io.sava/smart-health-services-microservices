package com.fii.appointment_service.dtos;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
public class AppointmentDto {
    @NotNull(message = "Date cannot be null")
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm")
    private LocalDateTime date;

    private String diagnosis;

    @NotNull(message = "PatientId cannot be null")
    private Long patientId;

    @NotNull(message = "NurseId cannot be null")
    private Long nurseId;

    @NotNull(message = "DoctorId cannot be null")
    private Long doctorId;
}
