package com.fii.appointment_service.dtos;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class PrescriptionDto {
    @NotNull(message = "Dose cannot be null")
    private String dose;

    @NotNull(message = "MedicationId cannot be null")
    private Long medicationId;

    @NotNull(message = "AppointmentId cannot be null")
    private Long appointmentId;
}
