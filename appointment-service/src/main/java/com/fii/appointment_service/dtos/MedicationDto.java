package com.fii.appointment_service.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MedicationDto {
    @NotNull(message = "Name cannot be null")
    private String name;

    @NotNull(message = "Brand cannot be null")
    private String brand;
}
