package com.fii.appointment_service.repositories;

import com.fii.appointment_service.entities.Nurse;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NurseRepository extends JpaRepository<Nurse, Long> {
}
