package com.fii.appointment_service.repositories;

import com.fii.appointment_service.entities.Hospital;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HospitalRepository extends JpaRepository<Hospital, Long> {
}
