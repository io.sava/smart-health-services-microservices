package com.fii.appointment_service.repositories;

import com.fii.appointment_service.entities.Doctor;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DoctorRepository extends JpaRepository<Doctor, Long> {
}
