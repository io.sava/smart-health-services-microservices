package com.fii.appointment_service.repositories;

import com.fii.appointment_service.entities.Patient;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PatientRepository extends JpaRepository<Patient, Long> {
}
