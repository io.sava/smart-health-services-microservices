package com.fii.appointment_service.repositories;

import com.fii.appointment_service.entities.Medication;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MedicationRepository extends JpaRepository<Medication, Long> {
}
