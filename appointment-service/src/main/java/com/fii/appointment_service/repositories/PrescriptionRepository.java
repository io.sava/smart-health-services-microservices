package com.fii.appointment_service.repositories;

import com.fii.appointment_service.entities.Prescription;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PrescriptionRepository extends JpaRepository<Prescription, Long> {
}
