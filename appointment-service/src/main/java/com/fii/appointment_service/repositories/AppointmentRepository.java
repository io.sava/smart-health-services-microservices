package com.fii.appointment_service.repositories;

import com.fii.appointment_service.entities.Appointment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AppointmentRepository extends JpaRepository<Appointment, Long> {
}
