package com.fii.appointment_service.services.rabbitmq;

import com.fii.appointment_service.entities.Hospital;
import com.fii.appointment_service.services.HospitalService;
import lombok.AllArgsConstructor;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.annotation.RabbitListenerConfigurer;
import org.springframework.amqp.rabbit.listener.RabbitListenerEndpointRegistrar;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class HospitalRabbitMQReceiver implements RabbitListenerConfigurer {
    private final HospitalService hospitalService;

    @Override
    public void configureRabbitListeners(RabbitListenerEndpointRegistrar rabbitListenerEndpointRegistrar) {

    }

    @RabbitListener(queues = "${rabbitmq.queue.create-hospital}")
    public void receiveCreateHospitalMessage(Hospital hospital) {
        hospitalService.create(hospital);
    }

    @RabbitListener(queues = "${rabbitmq.queue.update-hospital}")
    public void receiveUpdateHospitalMessage(Hospital hospital) {
        hospitalService.update(hospital);
    }

    @RabbitListener(queues = "${rabbitmq.queue.remove-hospital}")
    public void receiveRemoveHospitalMessage(Long hospitalId) {
        hospitalService.remove(hospitalId);
    }
}
