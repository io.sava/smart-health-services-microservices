package com.fii.appointment_service.services;

import com.fii.appointment_service.entities.Department;
import com.fii.appointment_service.repositories.DepartmentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class DepartmentService {
    private final DepartmentRepository departmentRepository;

    public void create(Department department) {
        departmentRepository.save(department);
    }

    public void update(Department department) {
        departmentRepository.save(department);
    }

    public void remove(Long departmentId) {
        departmentRepository.deleteById(departmentId);
    }
}
