package com.fii.appointment_service.services;

import com.fii.appointment_service.entities.Nurse;
import com.fii.appointment_service.exceptions.EntityNotFoundException;
import com.fii.appointment_service.repositories.NurseRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class NurseService {
    private static final String NURSE_ENTITY = "Nurse";
    private final NurseRepository nurseRepository;

    public Nurse getById(Long id) {
        return nurseRepository.findById(id).orElseThrow(() -> new EntityNotFoundException(NURSE_ENTITY, id));
    }

    public void create(Nurse nurse) {
        nurseRepository.save(nurse);
    }

    public void update(Nurse nurse) {
        nurseRepository.save(nurse);
    }

    public void remove(Long nurseId) {
        nurseRepository.deleteById(nurseId);
    }
}
