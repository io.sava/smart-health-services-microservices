package com.fii.appointment_service.services;

import com.fii.appointment_service.entities.Hospital;
import com.fii.appointment_service.repositories.HospitalRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class HospitalService {
    private final HospitalRepository hospitalRepository;

    public void create(Hospital hospital) {
        hospitalRepository.save(hospital);
    }

    public void update(Hospital hospital) {
        hospitalRepository.save(hospital);
    }

    public void remove(Long hospitalId) {
        hospitalRepository.deleteById(hospitalId);
    }
}
