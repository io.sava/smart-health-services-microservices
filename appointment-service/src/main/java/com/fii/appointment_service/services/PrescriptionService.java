package com.fii.appointment_service.services;

import com.fii.appointment_service.dtos.PrescriptionDto;
import com.fii.appointment_service.entities.Appointment;
import com.fii.appointment_service.entities.Prescription;
import com.fii.appointment_service.entities.Role;
import com.fii.appointment_service.entities.User;
import com.fii.appointment_service.exceptions.EntityNotFoundException;
import com.fii.appointment_service.mappers.PrescriptionMapper;
import com.fii.appointment_service.repositories.PrescriptionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
@RequiredArgsConstructor
public class PrescriptionService {
    private final PrescriptionRepository prescriptionRepository;
    private final PrescriptionMapper prescriptionMapper;
    private final AppointmentService appointmentService;

    public Prescription getById(Long id) {
        return prescriptionRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Prescription", id));
    }

    public List<Prescription> getByAppointmentId(Long appointmentId) {
        appointmentService.getById(appointmentId);

        return prescriptionRepository.findAll().stream()
                .filter(prescription -> prescription.getAppointment().getId().equals(appointmentId))
                .collect(Collectors.toList());
    }

    public void create(PrescriptionDto prescriptionDto) {
        prescriptionRepository.save(prescriptionMapper.toPrescription(prescriptionDto));
    }

    public void remove(Long id) {
        Prescription prescription = this.getById(id);
        prescriptionRepository.delete(prescription);
    }

    public boolean canUserCreatePrescription(User user, PrescriptionDto prescriptionDto) {
        if (user.getRole().equals(Role.DOCTOR)) {
            Appointment appointment = appointmentService.getById(prescriptionDto.getAppointmentId());
            return user.getOwnerId().equals(appointment.getDoctor().getId());
        }

        if (user.getRole().equals(Role.NURSE)) {
            Appointment appointment = appointmentService.getById(prescriptionDto.getAppointmentId());
            return user.getOwnerId().equals(appointment.getNurse().getId());
        }

        return false;
    }

    public boolean canUserDeletePrescription(User user, Long prescriptionId) {
        if (user.getRole().equals(Role.DOCTOR) || user.getRole().equals(Role.NURSE)) {
            Prescription prescription = this.getById(prescriptionId);
            if (user.getRole().equals(Role.DOCTOR)) {
                return user.getOwnerId().equals(prescription.getAppointment().getDoctor().getId());
            } else {
                return user.getOwnerId().equals(prescription.getAppointment().getNurse().getId());
            }
        }

        return false;
    }
}
