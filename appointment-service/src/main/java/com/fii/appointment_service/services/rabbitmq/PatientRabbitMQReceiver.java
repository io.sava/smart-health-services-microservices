package com.fii.appointment_service.services.rabbitmq;

import com.fii.appointment_service.entities.Patient;
import com.fii.appointment_service.services.PatientService;
import lombok.AllArgsConstructor;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.annotation.RabbitListenerConfigurer;
import org.springframework.amqp.rabbit.listener.RabbitListenerEndpointRegistrar;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class PatientRabbitMQReceiver implements RabbitListenerConfigurer {
    private final PatientService patientService;

    @Override
    public void configureRabbitListeners(RabbitListenerEndpointRegistrar rabbitListenerEndpointRegistrar) {

    }

    @RabbitListener(queues = "${rabbitmq.queue.create-patient}")
    public void receiveCreatePatientMessage(Patient patient) {
        patientService.create(patient);
    }

    @RabbitListener(queues = "${rabbitmq.queue.update-patient}")
    public void receiveUpdatePatientMessage(Patient patient) {
        patientService.update(patient);
    }

    @RabbitListener(queues = "${rabbitmq.queue.remove-patient}")
    public void receiveRemovePatientMessage(Long patientId) {
        patientService.remove(patientId);
    }
}
