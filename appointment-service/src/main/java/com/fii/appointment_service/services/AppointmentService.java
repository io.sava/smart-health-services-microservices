package com.fii.appointment_service.services;

import com.fii.appointment_service.dtos.AppointmentDto;
import com.fii.appointment_service.entities.Appointment;
import com.fii.appointment_service.entities.Doctor;
import com.fii.appointment_service.entities.Nurse;
import com.fii.appointment_service.entities.Prescription;
import com.fii.appointment_service.entities.Role;
import com.fii.appointment_service.entities.User;
import com.fii.appointment_service.exceptions.AppointmentWithoutPrescriptionsException;
import com.fii.appointment_service.exceptions.EntityNotFoundException;
import com.fii.appointment_service.mappers.AppointmentMapper;
import com.fii.appointment_service.repositories.AppointmentRepository;
import com.fii.appointment_service.repositories.PrescriptionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
@RequiredArgsConstructor
public class AppointmentService {
    private final AppointmentRepository appointmentRepository;
    private final PrescriptionRepository prescriptionRepository;
    private final AppointmentMapper appointmentMapper;
    private final DoctorService doctorService;
    private final NurseService nurseService;
    private final PatientService patientService;
    private final PrescriptionPDFExportService prescriptionPDFExportService;

    public Appointment getById(Long id) {
        return appointmentRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Appointment", id));
    }

    public List<Appointment> getByPatientId(Long patientId) {
        patientService.checkIfExists(patientId);

        return appointmentRepository.findAll().stream()
                .filter(appointment -> appointment.getPatient().getId().equals(patientId))
                .collect(Collectors.toList());
    }

    public void exportPrescriptions(Long id, HttpServletResponse response) throws IOException {
        Appointment appointment = this.getById(id);
        if (appointment.getPrescriptions().isEmpty()) {
            throw new AppointmentWithoutPrescriptionsException(id);
        }
        prescriptionPDFExportService.export(response, appointment);
    }

    public void create(AppointmentDto appointmentDto) {
        appointmentRepository.save(appointmentMapper.toAppointment(appointmentDto));
    }

    public void update(Long id, AppointmentDto appointmentDto) {
        Appointment appointmentToUpdate = appointmentMapper.toAppointment(appointmentDto);
        appointmentToUpdate.setId(id);
        appointmentRepository.save(appointmentToUpdate);
    }

    public void remove(Long id) {
        Appointment appointment = this.getById(id);
        for (Prescription prescription : appointment.getPrescriptions()) {
            prescriptionRepository.delete(prescription);
        }
        appointmentRepository.delete(appointment);
    }

    public boolean canUserGetAppointment(User user, Long appointmentId) {
        if (user.getRole().equals(Role.PATIENT)) {
            Appointment appointment = this.getById(appointmentId);
            return user.getOwnerId().equals(appointment.getPatient().getId());
        }
        return true;
    }

    public boolean canUserCreateAppointment(User user, AppointmentDto appointmentDto) {
        if (user.getRole().equals(Role.NURSE)) {
            if (!user.getOwnerId().equals(appointmentDto.getNurseId())) {
                return false;
            }

            Nurse nurse = nurseService.getById(user.getOwnerId());
            Doctor doctor = doctorService.getById(appointmentDto.getDoctorId());
            return nurse.getDepartment().getHospital().getId().equals(doctor.getDepartment().getHospital().getId());
        }
        return false;
    }

    public boolean canUserUpdateAppointment(User user, Long appointmentId, AppointmentDto appointmentDto) {
        if (user.getRole().equals(Role.NURSE)) {
            if (!user.getOwnerId().equals(appointmentDto.getNurseId())) {
                return false;
            }

            Appointment appointment = this.getById(appointmentId);
            if (appointment.getNurse().getId().equals(user.getOwnerId())) {
                Nurse nurse = nurseService.getById(user.getOwnerId());
                Doctor doctor = doctorService.getById(appointmentDto.getDoctorId());
                return nurse.getDepartment().getHospital().getId().equals(doctor.getDepartment().getHospital().getId());
            }
        }
        return false;
    }

    public boolean canUserDeleteAppointment(User user, Long appointmentId) {
        if (user.getRole().equals(Role.NURSE)) {
            Appointment appointment = this.getById(appointmentId);
            return appointment.getNurse().getId().equals(user.getOwnerId());
        }
        return user.getRole().equals(Role.ADMIN);
    }
}
