package com.fii.appointment_service.services;

import com.fii.appointment_service.entities.Appointment;
import com.fii.appointment_service.entities.Doctor;
import com.fii.appointment_service.entities.Hospital;
import com.fii.appointment_service.entities.Patient;
import com.fii.appointment_service.entities.Prescription;
import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.io.IOException;

@Service
public class PrescriptionPDFExportService {
    private void writeHospitalDetails(Document document, Hospital hospital) {
        Font font = FontFactory.getFont(FontFactory.HELVETICA);
        font.setSize(10);
        font.setColor(Color.BLACK);

        Paragraph name = new Paragraph(String.format("Unitatea sanitara: %s", hospital.getName()), font);
        name.setAlignment(Element.ALIGN_LEFT);
        document.add(name);

        Paragraph address = new Paragraph(String.format("Adresa: %s", hospital.getAddress()), font);
        address.setAlignment(Element.ALIGN_LEFT);
        address.setSpacingAfter(10);
        document.add(address);
    }

    private void writeTitle(Document document) {
        Font font = FontFactory.getFont(FontFactory.HELVETICA_BOLD);
        font.setSize(18);
        font.setColor(Color.BLACK);

        Paragraph title = new Paragraph("Reteta medicala", font);
        title.setAlignment(Element.ALIGN_CENTER);
        title.setSpacingAfter(15);
        document.add(title);
    }

    private void writePatientDetails(Document document, Patient patient, String diagnosis) {
        Font font = FontFactory.getFont(FontFactory.HELVETICA);
        font.setSize(10);
        font.setColor(Color.BLACK);

        Paragraph name = new Paragraph(String.format("Nume si prenume pacient: %s", patient.getName()), font);
        name.setAlignment(Element.ALIGN_LEFT);
        name.setSpacingAfter(5);
        document.add(name);

        Paragraph cnp = new Paragraph(String.format("CNP: %s", patient.getCnp()), font);
        cnp.setAlignment(Element.ALIGN_LEFT);
        cnp.setSpacingAfter(5);
        document.add(cnp);

        Paragraph address = new Paragraph(String.format("Domiciliu: %s", patient.getAddress()), font);
        address.setAlignment(Element.ALIGN_LEFT);
        address.setSpacingAfter(5);
        document.add(address);

        Paragraph age = new Paragraph(String.format("Varsta: %d", patient.getAge()), font);
        age.setAlignment(Element.ALIGN_LEFT);
        age.setSpacingAfter(5);
        document.add(age);

        Paragraph diagnosisText = new Paragraph(String.format("Diagnostic: %s", diagnosis), font);
        diagnosisText.setAlignment(Element.ALIGN_LEFT);
        diagnosisText.setSpacingAfter(5);
        document.add(diagnosisText);
    }

    private void writeTableHeader(PdfPTable table) {
        PdfPCell cell = new PdfPCell();
        cell.setBackgroundColor(Color.BLACK);
        cell.setPadding(5);

        Font font = FontFactory.getFont(FontFactory.HELVETICA);
        font.setColor(Color.WHITE);

        cell.setPhrase(new Phrase("Medicament", font));
        table.addCell(cell);

        cell.setPhrase(new Phrase("Doza", font));
        table.addCell(cell);
    }

    private void writeDoctorDetails(Document document, Doctor doctor) {
        Font font = FontFactory.getFont(FontFactory.HELVETICA);
        font.setSize(10);
        font.setColor(Color.BLACK);

        Paragraph name = new Paragraph(String.format("Doctor: %s", doctor.getName()), font);
        name.setAlignment(Element.ALIGN_LEFT);
        document.add(name);
    }

    private void writeTableData(PdfPTable table, Appointment appointment) {
        for (Prescription prescription : appointment.getPrescriptions()) {
            table.addCell(String.format("%s, %s", prescription.getMedication().getName(), prescription.getMedication().getBrand()));
            table.addCell(prescription.getDose());
        }
    }

    public void export(HttpServletResponse response, Appointment appointment) throws IOException {
        try (Document document = new Document(PageSize.A5)) {
            PdfWriter.getInstance(document, response.getOutputStream());
            document.open();

            writeHospitalDetails(document, appointment.getDoctor().getDepartment().getHospital());
            writeTitle(document);
            writePatientDetails(document, appointment.getPatient(), appointment.getDiagnosis());

            PdfPTable table = new PdfPTable(2);
            table.setWidthPercentage(100f);
            table.setWidths(new float[]{7.0f, 6.0f});
            table.setSpacingBefore(30);
            table.setSpacingAfter(30);
            writeTableHeader(table);
            writeTableData(table, appointment);
            document.add(table);

            writeDoctorDetails(document, appointment.getDoctor());
        }
    }
}
