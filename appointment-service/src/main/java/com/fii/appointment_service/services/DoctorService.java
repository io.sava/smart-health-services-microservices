package com.fii.appointment_service.services;

import com.fii.appointment_service.entities.Doctor;
import com.fii.appointment_service.exceptions.EntityNotFoundException;
import com.fii.appointment_service.repositories.DoctorRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class DoctorService {
    private static final String DOCTOR_ENTITY = "Doctor";
    private final DoctorRepository doctorRepository;

    public Doctor getById(Long id) {
        return doctorRepository.findById(id).orElseThrow(() -> new EntityNotFoundException(DOCTOR_ENTITY, id));
    }

    public void create(Doctor doctor) {
        doctorRepository.save(doctor);
    }

    public void update(Doctor doctor) {
        doctorRepository.save(doctor);
    }

    public void remove(Long doctorId) {
        doctorRepository.deleteById(doctorId);
    }
}
