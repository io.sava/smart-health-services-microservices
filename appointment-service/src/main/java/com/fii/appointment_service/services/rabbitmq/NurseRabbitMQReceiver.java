package com.fii.appointment_service.services.rabbitmq;

import com.fii.appointment_service.entities.Nurse;
import com.fii.appointment_service.services.NurseService;
import lombok.AllArgsConstructor;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.annotation.RabbitListenerConfigurer;
import org.springframework.amqp.rabbit.listener.RabbitListenerEndpointRegistrar;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class NurseRabbitMQReceiver implements RabbitListenerConfigurer {
    private final NurseService nurseService;

    @Override
    public void configureRabbitListeners(RabbitListenerEndpointRegistrar rabbitListenerEndpointRegistrar) {

    }

    @RabbitListener(queues = "${rabbitmq.queue.create-nurse}")
    public void receiveCreateNurseMessage(Nurse nurse) {
        nurseService.create(nurse);
    }

    @RabbitListener(queues = "${rabbitmq.queue.update-nurse}")
    public void receiveUpdateNurseMessage(Nurse nurse) {
        nurseService.update(nurse);
    }

    @RabbitListener(queues = "${rabbitmq.queue.remove-nurse}")
    public void receiveRemoveNurseMessage(Long nurseId) {
        nurseService.remove(nurseId);
    }
}
