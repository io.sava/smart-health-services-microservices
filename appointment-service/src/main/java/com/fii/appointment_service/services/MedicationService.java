package com.fii.appointment_service.services;

import com.fii.appointment_service.dtos.MedicationDto;
import com.fii.appointment_service.entities.Medication;
import com.fii.appointment_service.exceptions.EntityNotFoundException;
import com.fii.appointment_service.mappers.MedicationMapper;
import com.fii.appointment_service.repositories.MedicationRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class MedicationService {
    private final MedicationRepository medicationRepository;
    private final MedicationMapper medicationMapper;

    public List<Medication> getAll() {
        return medicationRepository.findAll();
    }

    public Medication getById(Long id) {
        return medicationRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Medication", id));
    }

    public void create(MedicationDto medicationDto) {
        medicationRepository.save(medicationMapper.toMedication(medicationDto));
    }

    public void remove(Long id) {
        Medication medication = this.getById(id);
        medicationRepository.delete(medication);
    }
}
