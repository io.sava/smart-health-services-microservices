package com.fii.appointment_service.configuration.rabbitmq;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.core.ExchangeBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class HospitalRabbitMQConfig {
    @Value("${spring.rabbitmq.template.hospital-exchange}")
    private String hospitalExchange;

    @Value("${rabbitmq.queue.create-hospital}")
    private String createHospitalQueue;

    @Value("${rabbitmq.routing-key.create-hospital}")
    private String createHospitalRoutingKey;

    @Value("${rabbitmq.queue.update-hospital}")
    private String updateHospitalQueue;

    @Value("${rabbitmq.routing-key.update-hospital}")
    private String updateHospitalRoutingKey;

    @Value("${rabbitmq.queue.remove-hospital}")
    private String removeHospitalQueue;

    @Value("${rabbitmq.routing-key.remove-hospital}")
    private String removeHospitalRoutingKey;

    @Bean
    public Exchange getHospitalExchange() {
        return ExchangeBuilder.directExchange(hospitalExchange).durable(true).build();
    }

    @Bean
    public Queue createHospitalQueue() {
        return new Queue(createHospitalQueue, true);
    }

    @Bean
    public Binding createHospitalBinding() {
        return BindingBuilder
                .bind(createHospitalQueue())
                .to(getHospitalExchange())
                .with(createHospitalRoutingKey)
                .noargs();
    }

    @Bean
    public Queue updateHospitalQueue() {
        return new Queue(updateHospitalQueue, true);
    }

    @Bean
    public Binding updateHospitalBinding() {
        return BindingBuilder
                .bind(updateHospitalQueue())
                .to(getHospitalExchange())
                .with(updateHospitalRoutingKey)
                .noargs();
    }

    @Bean
    public Queue removeHospitalQueue() {
        return new Queue(removeHospitalQueue, true);
    }

    @Bean
    public Binding removeHospitalBinding() {
        return BindingBuilder
                .bind(removeHospitalQueue())
                .to(getHospitalExchange())
                .with(removeHospitalRoutingKey)
                .noargs();
    }
}
