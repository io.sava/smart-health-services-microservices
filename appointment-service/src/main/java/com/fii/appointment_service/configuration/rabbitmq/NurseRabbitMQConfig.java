package com.fii.appointment_service.configuration.rabbitmq;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.core.ExchangeBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class NurseRabbitMQConfig {
    @Value("${spring.rabbitmq.template.nurse-exchange}")
    private String nurseExchange;

    @Value("${rabbitmq.queue.create-nurse}")
    private String createNurseQueue;

    @Value("${rabbitmq.routing-key.create-nurse}")
    private String createNurseRoutingKey;

    @Value("${rabbitmq.queue.update-nurse}")
    private String updateNurseQueue;

    @Value("${rabbitmq.routing-key.update-nurse}")
    private String updateNurseRoutingKey;

    @Value("${rabbitmq.queue.remove-nurse}")
    private String removeNurseQueue;

    @Value("${rabbitmq.routing-key.remove-nurse}")
    private String removeNurseRoutingKey;

    @Bean
    public Exchange getNurseExchange() {
        return ExchangeBuilder.directExchange(nurseExchange).durable(true).build();
    }

    @Bean
    public Queue createNurseQueue() {
        return new Queue(createNurseQueue, true);
    }

    @Bean
    public Binding createNurseBinding() {
        return BindingBuilder
                .bind(createNurseQueue())
                .to(getNurseExchange())
                .with(createNurseRoutingKey)
                .noargs();
    }

    @Bean
    public Queue updateNurseQueue() {
        return new Queue(updateNurseQueue, true);
    }

    @Bean
    public Binding updateNurseBinding() {
        return BindingBuilder
                .bind(updateNurseQueue())
                .to(getNurseExchange())
                .with(updateNurseRoutingKey)
                .noargs();
    }

    @Bean
    public Queue removeNurseQueue() {
        return new Queue(removeNurseQueue, true);
    }

    @Bean
    public Binding removeNurseBinding() {
        return BindingBuilder
                .bind(removeNurseQueue())
                .to(getNurseExchange())
                .with(removeNurseRoutingKey)
                .noargs();
    }
}
