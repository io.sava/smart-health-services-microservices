package com.fii.appointment_service.configuration.rabbitmq;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.core.ExchangeBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PatientRabbitMQConfig {
    @Value("${spring.rabbitmq.template.patient-exchange}")
    private String patientExchange;

    @Value("${rabbitmq.queue.create-patient}")
    private String createPatientQueue;

    @Value("${rabbitmq.routing-key.create-patient}")
    private String createPatientRoutingKey;

    @Value("${rabbitmq.queue.update-patient}")
    private String updatePatientQueue;

    @Value("${rabbitmq.routing-key.update-patient}")
    private String updatePatientRoutingKey;

    @Value("${rabbitmq.queue.remove-patient}")
    private String removePatientQueue;

    @Value("${rabbitmq.routing-key.remove-patient}")
    private String removePatientRoutingKey;

    @Bean
    public Exchange getPatientExchange() {
        return ExchangeBuilder.directExchange(patientExchange).durable(true).build();
    }

    @Bean
    public Queue createPatientQueue() {
        return new Queue(createPatientQueue, true);
    }

    @Bean
    public Binding createPatientBinding() {
        return BindingBuilder
                .bind(createPatientQueue())
                .to(getPatientExchange())
                .with(createPatientRoutingKey)
                .noargs();
    }

    @Bean
    public Queue updatePatientQueue() {
        return new Queue(updatePatientQueue, true);
    }

    @Bean
    public Binding updatePatientBinding() {
        return BindingBuilder
                .bind(updatePatientQueue())
                .to(getPatientExchange())
                .with(updatePatientRoutingKey)
                .noargs();
    }

    @Bean
    public Queue removePatientQueue() {
        return new Queue(removePatientQueue, true);
    }

    @Bean
    public Binding removePatientBinding() {
        return BindingBuilder
                .bind(removePatientQueue())
                .to(getPatientExchange())
                .with(removePatientRoutingKey)
                .noargs();
    }
}
