package com.fii.appointment_service.entities;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@EqualsAndHashCode(callSuper = false)
@Data
@Entity
@Table(name = "nurses")
public class Nurse extends BaseEntity {
    private String name;

    @ManyToOne
    @JoinColumn(name = "department_id")
    private Department department;
}
