package com.fii.appointment_service.exceptions;

public class AppointmentWithoutPrescriptionsException extends RuntimeException {
    public AppointmentWithoutPrescriptionsException(Long appointmentId) {
        super(String.format("Appointment with id %d has no prescriptions", appointmentId));
    }
}
