package com.fii.appointment_service.mappers;

import com.fii.appointment_service.dtos.MedicationDto;
import com.fii.appointment_service.entities.Medication;
import org.springframework.stereotype.Component;

@Component
public final class MedicationMapper {
    public Medication toMedication(MedicationDto medicationDto) {
        Medication medication = new Medication();
        medication.setName(medicationDto.getName());
        medication.setBrand(medicationDto.getBrand());

        return medication;
    }
}
