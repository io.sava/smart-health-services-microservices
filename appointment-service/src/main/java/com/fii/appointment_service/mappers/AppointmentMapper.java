package com.fii.appointment_service.mappers;

import com.fii.appointment_service.dtos.AppointmentDto;
import com.fii.appointment_service.entities.Appointment;
import com.fii.appointment_service.services.DoctorService;
import com.fii.appointment_service.services.NurseService;
import com.fii.appointment_service.services.PatientService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public final class AppointmentMapper {
    private final DoctorService doctorService;
    private final NurseService nurseService;
    private final PatientService patientService;

    public Appointment toAppointment(AppointmentDto appointmentDto) {
        Appointment appointment = new Appointment();
        appointment.setDate(appointmentDto.getDate());
        appointment.setDiagnosis(appointmentDto.getDiagnosis());
        appointment.setDoctor(doctorService.getById(appointmentDto.getDoctorId()));
        appointment.setNurse(nurseService.getById(appointmentDto.getNurseId()));
        appointment.setPatient(patientService.getById(appointmentDto.getPatientId()));

        return appointment;
    }
}
