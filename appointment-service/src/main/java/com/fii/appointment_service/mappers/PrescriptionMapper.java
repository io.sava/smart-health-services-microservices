package com.fii.appointment_service.mappers;

import com.fii.appointment_service.dtos.PrescriptionDto;
import com.fii.appointment_service.entities.Prescription;
import com.fii.appointment_service.services.AppointmentService;
import com.fii.appointment_service.services.MedicationService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public final class PrescriptionMapper {
    private final AppointmentService appointmentService;
    private final MedicationService medicationService;

    public Prescription toPrescription(PrescriptionDto prescriptionDto) {
        Prescription prescription = new Prescription();
        prescription.setDose(prescriptionDto.getDose());
        prescription.setAppointment(appointmentService.getById(prescriptionDto.getAppointmentId()));
        prescription.setMedication(medicationService.getById(prescriptionDto.getMedicationId()));

        return prescription;
    }
}
