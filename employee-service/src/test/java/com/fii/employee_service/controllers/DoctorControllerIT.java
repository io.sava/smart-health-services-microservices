package com.fii.employee_service.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fii.employee_service.dtos.DoctorDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
@RunWith(SpringRunner.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class DoctorControllerIT {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper mapper;

    @Test
    public void getDoctorWithId_1_shouldReturnOk_whenDoctorWithId_1_isInDatabase() throws Exception {
        Long doctorId = 1L;

        this.mockMvc.perform(get("/api/v1/doctors/{id}", doctorId))
                .andExpect(status().isOk());
    }

    @Test
    public void getDoctorWithId_100_shouldReturnNotFound_whenDoctorWithId_100_isNotInDatabase() throws Exception {
        Long doctorId = 100L;

        this.mockMvc.perform(get("/api/v1/doctors/{id}", doctorId))
                .andExpect(status().isNotFound());
    }

    @Test
    public void getDoctorsByDepartment_shouldReturnOk_whenDepartmentWithId_1_isInDatabase() throws Exception {
        Long departmentId = 1L;

        this.mockMvc.perform(get("/api/v1/doctors/departments/{departmentId}", departmentId))
                .andExpect(status().isOk());
    }

    @Test
    public void getDoctorsByDepartment_shouldReturnNotFound_whenDepartmentWithId_100_isNotInDatabase() throws Exception {
        Long departmentId = 100L;

        this.mockMvc.perform(get("/api/v1/doctors/departments/{departmentId}", departmentId))
                .andExpect(status().isNotFound());
    }

    @Test
    public void createDoctor_shouldReturnCreated() throws Exception {
        DoctorDto doctor = new DoctorDto("Costel Negreanu", "Medic specialist", "doctor@test.com", 1L);

        this.mockMvc.perform(post("/api/v1/doctors")
                .content(mapper.writeValueAsString(doctor))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    @Test
    public void createDoctor_shouldReturnNotFound_whenDepartmentWithId_100_isNotInDatabase() throws Exception {
        DoctorDto doctor = new DoctorDto("Costel Negreanu", "Medic specialist", "doctor@test.com", 100L);

        this.mockMvc.perform(post("/api/v1/doctors")
                .content(mapper.writeValueAsString(doctor))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void updateDoctorWithId_1_shouldReturnNoContent_whenDoctorWithId_1_isInDatabase() throws Exception {
        Long doctorId = 1L;
        DoctorDto doctor = new DoctorDto("Vasile Negreanu", "Medic specialist", "doctor@test.com", 1L);

        this.mockMvc.perform(put("/api/v1/doctors/{id}", doctorId)
                .content(mapper.writeValueAsString(doctor))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }

    @Test
    public void updateDoctorWithId_1_shouldReturnNotFound_whenDepartmentWithId_100_isNotInDatabase() throws Exception {
        Long doctorId = 1L;
        DoctorDto doctor = new DoctorDto("Vasile Negreanu", "Medic specialist", "doctor@test.com", 100L);

        this.mockMvc.perform(put("/api/v1/doctors/{id}", doctorId)
                .content(mapper.writeValueAsString(doctor))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void updateDoctorWithId_100_shouldReturnNotFound_whenDoctorWithId_100_isNotInDatabase() throws Exception {
        Long doctorId = 100L;
        DoctorDto doctor = new DoctorDto("Vasile Negreanu", "Medic specialist", "doctor@test.com", 2L);

        this.mockMvc.perform(put("/api/v1/doctors/{id}", doctorId)
                .content(mapper.writeValueAsString(doctor))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void deleteDoctorWithId_1_shouldReturnNoContent_whenDoctorWithId_1_isInDatabase() throws Exception {
        Long doctorId = 1L;

        this.mockMvc.perform(delete("/api/v1/doctors/{id}", doctorId))
                .andExpect(status().isNoContent());
    }

    @Test
    public void deleteDoctorWithId_100_shouldReturnNotFound_whenDoctorWithId_100_isNotInDatabase() throws Exception {
        Long doctorId = 100L;

        this.mockMvc.perform(delete("/api/v1/doctors/{id}", doctorId))
                .andExpect(status().isNotFound());
    }
}
