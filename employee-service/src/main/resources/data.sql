-- departments
insert into departments(name, hospital_id)
values ('Medicina interna', 1);
insert into departments(name, hospital_id)
values ('Chirurgie generala', 1);
insert into departments(name, hospital_id)
values ('Ortopedie si traumatologie', 1);
insert into departments(name, hospital_id)
values ('Oftalmologie', 1);
insert into departments(name, hospital_id)
values ('ORL', 1);
insert into departments(name, hospital_id)
values ('Dermato-Venerologie', 1);

insert into departments(name, hospital_id)
values ('Medicina interna', 2);
insert into departments(name, hospital_id)
values ('Chirurgie generala', 2);
insert into departments(name, hospital_id)
values ('Ortopedie si traumatologie', 2);
insert into departments(name, hospital_id)
values ('Oftalmologie', 2);
insert into departments(name, hospital_id)
values ('ORL', 2);
insert into departments(name, hospital_id)
values ('Dermato-Venerologie', 2);

insert into departments(name, hospital_id)
values ('Medicina interna', 3);
insert into departments(name, hospital_id)
values ('Chirurgie generala', 3);
insert into departments(name, hospital_id)
values ('Ortopedie si traumatologie', 3);
insert into departments(name, hospital_id)
values ('Oftalmologie', 3);
insert into departments(name, hospital_id)
values ('ORL', 3);
insert into departments(name, hospital_id)
values ('Dermato-Venerologie', 3);

-- doctors
insert into doctors(name, position, email, department_id)
values ('Virgil Laurentiu', 'Medic specialist', 'virgil@gmail.com', 1);
insert into doctors(name, position, email, department_id)
values ('Mirel Titirez', 'Medic rezident', 'mirel@gmail.com', 2);
insert into doctors(name, position, email, department_id)
values ('Emanuel Gheorghe', 'Medic specialist', 'emanuel@gmail.com', 3);
insert into doctors(name, position, email, department_id)
values ('Arsenie Romanescu', 'Medic rezident', 'arsenie@gmail.com', 2);
insert into doctors(name, position, email, department_id)
values ('Iulien Grigorescu', 'Medic primar', 'iulien@gmail.com', 1);
insert into doctors(name, position, email, department_id)
values ('Nicusor Luca', 'Medic specialist', 'nicusor@gmail.com', 4);
insert into doctors(name, position, email, department_id)
values ('Calin Balcescu', 'Medic primar', 'calin@gmail.com', 5);
insert into doctors(name, position, email, department_id)
values ('Miron Stancu', 'Medic rezident', 'miron@gmail.com', 3);
insert into doctors(name, position, email, department_id)
values ('Jan Muresan', 'Medic specialist', 'jan@gmail.com', 6);

insert into doctors(name, position, email, department_id)
values ('Luca Dumitru', 'Medic specialist', 'luca@gmail.com', 7);
insert into doctors(name, position, email, department_id)
values ('Dorin Galca', 'Medic specialist', 'dorin@gmail.com', 7);
insert into doctors(name, position, email, department_id)
values ('Gabriel Enache', 'Medic rezident', 'gabriel@gmail.com', 8);
insert into doctors(name, position, email, department_id)
values ('Paul Puscas', 'Medic specialist', 'paul@gmail.com', 9);
insert into doctors(name, position, email, department_id)
values ('Bogdan Andreescu', 'Medic rezident', 'bogdan@gmail.com', 8);
insert into doctors(name, position, email, department_id)
values ('Matei Barbu', 'Medic primar', 'matei@gmail.com', 7);
insert into doctors(name, position, email, department_id)
values ('Toma Zamfir', 'Medic specialist', 'toma@gmail.com', 10);
insert into doctors(name, position, email, department_id)
values ('Sebastian Raceanu', 'Medic primar', 'sebastian@gmail.com', 11);
insert into doctors(name, position, email, department_id)
values ('Teodosie Ilionescu', 'Medic rezident', 'teodosie@gmail.com', 9);
insert into doctors(name, position, email, department_id)
values ('Raul Plesu', 'Medic specialist', 'raul@gmail.com', 12);

insert into doctors(name, position, email, department_id)
values ('Sergiu Gheorghiu', 'Medic specialist', 'sergiu@gmail.com', 13);
insert into doctors(name, position, email, department_id)
values ('Cezar Lupul', 'Medic rezident', 'cezar@gmail.com', 13);
insert into doctors(name, position, email, department_id)
values ('Valerian Roman', 'Medic specialist', 'valerian@gmail.com', 14);
insert into doctors(name, position, email, department_id)
values ('Jean Negrescu', 'Medic rezident', 'jean@gmail.com', 15);
insert into doctors(name, position, email, department_id)
values ('Ion Pecurar', 'Medic primar', 'ion@gmail.com', 14);
insert into doctors(name, position, email, department_id)
values ('Serghei Alexandrescu', 'Medic specialist', 'serghei@gmail.com', 13);
insert into doctors(name, position, email, department_id)
values ('Geza Niculescu', 'Medic primar', 'geza@gmail.com', 16);
insert into doctors(name, position, email, department_id)
values ('Tudor Serban', 'Medic rezident', 'tudor@gmail.com', 17);
insert into doctors(name, position, email, department_id)
values ('Silviu Arcos', 'Medic specialist', 'silviu@gmail.com', 15);
insert into doctors(name, position, email, department_id)
values ('Costel Avramescu', 'Medic specialist', 'costel@gmail.com', 18);

-- nurses
insert into nurses(name, position, email, department_id)
values ('Angela Cornea', 'Asistent medical generalist', 'angela@gmail.com', 1);
insert into nurses(name, position, email, department_id)
values ('Ionela Gheorghe', 'Asistent medical generalist', 'ionela@gmail.com', 3);
insert into nurses(name, position, email, department_id)
values ('Miruna Vulpes', 'Asistent medical generalist', 'miruna@gmail.com', 7);
insert into nurses(name, position, email, department_id)
values ('Felicia Bogza', 'Asistent medical generalist', 'felicia@gmail.com', 10);
insert into nurses(name, position, email, department_id)
values ('Loredana Grigorescu', 'Asistent medical generalist', 'loredana@gmail.com', 13);
insert into nurses(name, position, email, department_id)
values ('Veronica Vasile', 'Asistent medical generalist', 'veronica@gmail.com', 14);
insert into nurses(name, position, email, department_id)
values ('Celestina Tugurlan', 'Asistent medical generalist', 'celestina@gmail.com', 16);