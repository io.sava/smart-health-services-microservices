package com.fii.employee_service.services.rabbitmq;

import com.fii.employee_service.dtos.account.AccountDetailsDto;
import com.fii.employee_service.dtos.account.UpdateAccountEmailDto;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserRabbitMQSender {
    private final RabbitTemplate rabbitTemplate;

    @Value("${spring.rabbitmq.template.user-exchange}")
    private String exchange;

    @Value("${rabbitmq.routing-key.create-account}")
    private String createAccountRoutingKey;

    @Value("${rabbitmq.routing-key.update-account-email}")
    private String updateAccountEmailRoutingKey;

    @Value("${rabbitmq.routing-key.remove-account-by-email}")
    private String removeAccountByEmailRoutingKey;

    public void sendCreateAccountMessage(AccountDetailsDto accountDetailsDto) {
        rabbitTemplate.convertAndSend(exchange, createAccountRoutingKey, accountDetailsDto);
    }

    public void sendUpdateAccountEmailMessage(UpdateAccountEmailDto updateAccountEmailDto) {
        rabbitTemplate.convertAndSend(exchange, updateAccountEmailRoutingKey, updateAccountEmailDto);
    }

    public void sendRemoveAccountByEmailMessage(String email) {
        rabbitTemplate.convertAndSend(exchange, removeAccountByEmailRoutingKey, email);
    }
}
