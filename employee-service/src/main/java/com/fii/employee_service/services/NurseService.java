package com.fii.employee_service.services;

import com.fii.employee_service.dtos.NurseDto;
import com.fii.employee_service.entities.Nurse;
import com.fii.employee_service.exceptions.EntityNotFoundException;
import com.fii.employee_service.mappers.NurseMapper;
import com.fii.employee_service.repositories.NurseRepository;
import com.fii.employee_service.services.rabbitmq.NurseRabbitMQSender;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
@RequiredArgsConstructor
public class NurseService {
    private static final String NURSE_ENTITY = "Nurse";
    private final NurseRepository nurseRepository;
    private final NurseMapper nurseMapper;
    private final UserService userService;
    private final DepartmentService departmentService;
    private final NurseRabbitMQSender nurseRabbitMQSender;

    public Page<Nurse> getAll(Pageable pageable) {
        return nurseRepository.findAll(pageable);
    }

    public Nurse getById(Long id) {
        return nurseRepository.findById(id).orElseThrow(() -> new EntityNotFoundException(NURSE_ENTITY, id));
    }

    public List<Nurse> getByDepartmentId(Long departmentId) {
        departmentService.checkIfExists(departmentId);

        return nurseRepository.findAll().stream()
                .filter(nurse -> nurse.getDepartment().getId().equals(departmentId))
                .collect(Collectors.toList());
    }

    public void create(NurseDto nurseDto) {
        departmentService.checkIfExists(nurseDto.getDepartmentId());
        Nurse nurse = nurseRepository.save(nurseMapper.toNurse(nurseDto));
        nurseRabbitMQSender.sendCreateNurseMessage(nurse);
        userService.generateAccount(nurse);
    }

    public void update(Long id, NurseDto nurseDto) {
        this.checkIfExists(id);
        departmentService.checkIfExists(nurseDto.getDepartmentId());
        Nurse nurseToUpdate = nurseMapper.toNurse(nurseDto);
        nurseToUpdate.setId(id);
        Nurse nurse = nurseRepository.save(nurseToUpdate);
        nurseRabbitMQSender.sendUpdateNurseMessage(nurse);
        userService.updateEmail(id, nurseDto.getEmail(), nurseToUpdate.getRole());
    }

    public void remove(Long id) {
        Nurse nurse = getById(id);
        nurseRepository.delete(nurse);
        nurseRabbitMQSender.sendRemoveNurseMessage(id);
        userService.removeByEmail(nurse.getEmail());
    }

    private void checkIfExists(Long id) {
        if (nurseRepository.findById(id).isEmpty()) {
            throw new EntityNotFoundException(NURSE_ENTITY, id);
        }
    }
}
