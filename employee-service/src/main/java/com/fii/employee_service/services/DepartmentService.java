package com.fii.employee_service.services;

import com.fii.employee_service.entities.Department;
import com.fii.employee_service.exceptions.EntityNotFoundException;
import com.fii.employee_service.repositories.DepartmentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class DepartmentService {
    private static final String DEPARTMENT_ENTITY = "Department";
    private final DepartmentRepository departmentRepository;

    public Department getById(Long id) {
        return departmentRepository.findById(id).orElseThrow(() -> new EntityNotFoundException(DEPARTMENT_ENTITY, id));
    }

    public void checkIfExists(Long id) {
        if (departmentRepository.findById(id).isEmpty()) {
            throw new EntityNotFoundException(DEPARTMENT_ENTITY, id);
        }
    }

    public void create(Department department) {
        departmentRepository.save(department);
    }

    public void update(Department department) {
        departmentRepository.save(department);
    }

    public void remove(Long departmentId) {
        departmentRepository.deleteById(departmentId);
    }
}
