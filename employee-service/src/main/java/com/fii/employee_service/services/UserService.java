package com.fii.employee_service.services;

import com.fii.employee_service.dtos.account.AccountDetailsDto;
import com.fii.employee_service.dtos.account.UpdateAccountEmailDto;
import com.fii.employee_service.entities.Employee;
import com.fii.employee_service.services.rabbitmq.UserRabbitMQSender;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserService {
    private final UserRabbitMQSender userRabbitMqSender;

    public void generateAccount(Employee employee) {
        userRabbitMqSender.sendCreateAccountMessage(getAccountDetails(employee));
    }

    public void updateEmail(Long employeeId, String email, String role) {
        userRabbitMqSender.sendUpdateAccountEmailMessage(new UpdateAccountEmailDto(employeeId, email, role));
    }

    public void removeByEmail(String email) {
        userRabbitMqSender.sendRemoveAccountByEmailMessage(email);
    }

    private AccountDetailsDto getAccountDetails(Employee employee) {
        return new AccountDetailsDto(employee.getId(), employee.getEmail(), employee.getRole());
    }
}
