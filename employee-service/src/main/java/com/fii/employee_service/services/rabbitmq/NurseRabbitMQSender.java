package com.fii.employee_service.services.rabbitmq;

import com.fii.employee_service.entities.Nurse;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class NurseRabbitMQSender {
    private final RabbitTemplate rabbitTemplate;

    @Value("${spring.rabbitmq.template.nurse-exchange}")
    private String exchange;

    @Value("${rabbitmq.routing-key.create-nurse}")
    private String createNurseRoutingKey;

    @Value("${rabbitmq.routing-key.update-nurse}")
    private String updateNurseRoutingKey;

    @Value("${rabbitmq.routing-key.remove-nurse}")
    private String removeNurseRoutingKey;

    public void sendCreateNurseMessage(Nurse nurse) {
        rabbitTemplate.convertAndSend(exchange, createNurseRoutingKey, nurse);
    }

    public void sendUpdateNurseMessage(Nurse nurse) {
        rabbitTemplate.convertAndSend(exchange, updateNurseRoutingKey, nurse);
    }

    public void sendRemoveNurseMessage(Long nurseId) {
        rabbitTemplate.convertAndSend(exchange, removeNurseRoutingKey, nurseId);
    }
}
