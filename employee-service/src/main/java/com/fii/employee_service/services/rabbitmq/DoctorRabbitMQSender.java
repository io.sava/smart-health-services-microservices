package com.fii.employee_service.services.rabbitmq;

import com.fii.employee_service.entities.Doctor;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class DoctorRabbitMQSender {
    private final RabbitTemplate rabbitTemplate;

    @Value("${spring.rabbitmq.template.doctor-exchange}")
    private String exchange;

    @Value("${rabbitmq.routing-key.create-doctor}")
    private String createDoctorRoutingKey;

    @Value("${rabbitmq.routing-key.update-doctor}")
    private String updateDoctorRoutingKey;

    @Value("${rabbitmq.routing-key.remove-doctor}")
    private String removeDoctorRoutingKey;

    public void sendCreateDoctorMessage(Doctor doctor) {
        rabbitTemplate.convertAndSend(exchange, createDoctorRoutingKey, doctor);
    }

    public void sendUpdateDoctorMessage(Doctor doctor) {
        rabbitTemplate.convertAndSend(exchange, updateDoctorRoutingKey, doctor);
    }

    public void sendRemoveDoctorMessage(Long doctorId) {
        rabbitTemplate.convertAndSend(exchange, removeDoctorRoutingKey, doctorId);
    }
}
