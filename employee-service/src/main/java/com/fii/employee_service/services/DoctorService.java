package com.fii.employee_service.services;

import com.fii.employee_service.dtos.DoctorDto;
import com.fii.employee_service.entities.Doctor;
import com.fii.employee_service.exceptions.EntityNotFoundException;
import com.fii.employee_service.mappers.DoctorMapper;
import com.fii.employee_service.repositories.DoctorRepository;
import com.fii.employee_service.services.rabbitmq.DoctorRabbitMQSender;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
@RequiredArgsConstructor
public class DoctorService {
    private static final String DOCTOR_ENTITY = "Doctor";
    private final DoctorRepository doctorRepository;
    private final DoctorMapper doctorMapper;
    private final UserService userService;
    private final DepartmentService departmentService;
    private final DoctorRabbitMQSender doctorRabbitMQSender;

    public Page<Doctor> getAll(Pageable pageable) {
        return doctorRepository.findAll(pageable);
    }

    public Doctor getById(Long id) {
        return doctorRepository.findById(id).orElseThrow(() -> new EntityNotFoundException(DOCTOR_ENTITY, id));
    }

    public List<Doctor> getByDepartmentId(Long departmentId) {
        departmentService.checkIfExists(departmentId);

        return doctorRepository.findAll().stream()
                .filter(doctor -> doctor.getDepartment().getId().equals(departmentId))
                .collect(Collectors.toList());
    }

    public void create(DoctorDto doctorDto) {
        departmentService.checkIfExists(doctorDto.getDepartmentId());
        Doctor doctor = doctorRepository.save(doctorMapper.toDoctor(doctorDto));
        doctorRabbitMQSender.sendCreateDoctorMessage(doctor);
        userService.generateAccount(doctor);
    }

    public void update(Long id, DoctorDto doctorDto) {
        this.checkIfExists(id);
        departmentService.checkIfExists(doctorDto.getDepartmentId());
        Doctor doctorToUpdate = doctorMapper.toDoctor(doctorDto);
        doctorToUpdate.setId(id);
        Doctor doctor = doctorRepository.save(doctorToUpdate);
        doctorRabbitMQSender.sendUpdateDoctorMessage(doctor);
        userService.updateEmail(id, doctorDto.getEmail(), doctorToUpdate.getRole());
    }

    public void remove(Long id) {
        Doctor doctor = getById(id);
        doctorRepository.delete(doctor);
        doctorRabbitMQSender.sendRemoveDoctorMessage(id);
        userService.removeByEmail(doctor.getEmail());
    }

    private void checkIfExists(Long id) {
        if (doctorRepository.findById(id).isEmpty()) {
            throw new EntityNotFoundException(DOCTOR_ENTITY, id);
        }
    }
}
