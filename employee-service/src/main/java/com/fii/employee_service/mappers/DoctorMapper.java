package com.fii.employee_service.mappers;

import com.fii.employee_service.dtos.DoctorDto;
import com.fii.employee_service.entities.Doctor;
import com.fii.employee_service.services.DepartmentService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public final class DoctorMapper {
    private final DepartmentService departmentService;

    public Doctor toDoctor(DoctorDto doctorDto) {
        Doctor doctor = new Doctor();
        doctor.setName(doctorDto.getName());
        doctor.setPosition(doctorDto.getPosition());
        doctor.setEmail(doctorDto.getEmail());
        doctor.setDepartment(departmentService.getById(doctorDto.getDepartmentId()));

        return doctor;
    }
}
