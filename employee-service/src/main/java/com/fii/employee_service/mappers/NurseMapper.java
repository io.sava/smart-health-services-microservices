package com.fii.employee_service.mappers;

import com.fii.employee_service.dtos.NurseDto;
import com.fii.employee_service.entities.Nurse;
import com.fii.employee_service.services.DepartmentService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public final class NurseMapper {
    private final DepartmentService departmentService;

    public Nurse toNurse(NurseDto nurseDto) {
        Nurse nurse = new Nurse();
        nurse.setName(nurseDto.getName());
        nurse.setPosition(nurseDto.getPosition());
        nurse.setEmail(nurseDto.getEmail());
        nurse.setDepartment(departmentService.getById(nurseDto.getDepartmentId()));

        return nurse;
    }
}
