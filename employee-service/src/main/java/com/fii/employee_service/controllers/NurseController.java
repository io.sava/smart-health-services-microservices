package com.fii.employee_service.controllers;

import com.fii.employee_service.dtos.NurseDto;
import com.fii.employee_service.entities.Nurse;
import com.fii.employee_service.services.NurseService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.util.List;

@RestController
@RequestMapping("api/v1/nurses")
@RequiredArgsConstructor
public class NurseController {
    private final NurseService nurseService;

    @GetMapping
    public Page<Nurse> getAll(Pageable pageable) {
        return nurseService.getAll(pageable);
    }

    @GetMapping("/{id}")
    public Nurse getById(@PathVariable @Valid @Min(0) Long id) {
        return nurseService.getById(id);
    }

    @GetMapping("/departments/{departmentId}")
    public List<Nurse> getByDepartmentId(@PathVariable @Valid @Min(0) Long departmentId) {
        return nurseService.getByDepartmentId(departmentId);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void create(@RequestBody @Valid NurseDto nurseDto) {
        nurseService.create(nurseDto);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void update(@PathVariable @Valid @Min(0) Long id, @RequestBody @Valid NurseDto nurseDto) {
        nurseService.update(id, nurseDto);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remove(@PathVariable @Valid @Min(0) Long id) {
        nurseService.remove(id);
    }
}