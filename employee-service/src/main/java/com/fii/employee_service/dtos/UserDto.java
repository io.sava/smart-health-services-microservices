package com.fii.employee_service.dtos;

import lombok.Data;

@Data
public class UserDto {
    private Long id;
    private String email;
    private String role;
}
