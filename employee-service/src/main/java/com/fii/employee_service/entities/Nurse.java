package com.fii.employee_service.entities;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Table;

@EqualsAndHashCode(callSuper = false)
@Data
@Entity
@Table(name = "nurses")
public class Nurse extends Employee {
    @Override
    public String getRole() {
        return "NURSE";
    }
}
