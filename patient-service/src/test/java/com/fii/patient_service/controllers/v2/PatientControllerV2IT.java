package com.fii.patient_service.controllers.v2;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fii.patient_service.cqrs.commands.CreatePatientCommand;
import com.fii.patient_service.cqrs.commands.UpdatePatientCommand;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
@RunWith(SpringRunner.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class PatientControllerV2IT {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper mapper;

    @Test
    public void getPatientWithId_1_shouldReturnOk_whenPatientWithId_1_isInDatabase() throws Exception {
        Long patientId = 1L;

        this.mockMvc.perform(get("/api/v2/patients/{id}", patientId))
                .andExpect(status().isOk());
    }

    @Test
    public void getPatientWithId_100_shouldReturnNotFound_whenPatientWithId_100_isNotInDatabase() throws Exception {
        Long patientId = 100L;

        this.mockMvc.perform(get("/api/v2/patients/{id}", patientId))
                .andExpect(status().isNotFound());
    }

    @Test
    public void createPatient_shouldReturnCreated() throws Exception {
        CreatePatientCommand createPatientCommand =
                new CreatePatientCommand("1990620336382", "Angel Costin", 21, "Pascani", "0741222222", "patient@test.com");

        this.mockMvc.perform(post("/api/v2/patients")
                .content(mapper.writeValueAsString(createPatientCommand))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    @Test
    public void updatePatientWithId_1_shouldReturnNoContent_whenPatientWithId_1_isInDatabase() throws Exception {
        Long patientId = 1L;
        UpdatePatientCommand updatePatientCommand =
                new UpdatePatientCommand(patientId, "1990620336382", "Angel Costin", 21, "Pascani", "0741222222", "patient@test.com");

        this.mockMvc.perform(put("/api/v1/patients")
                .content(mapper.writeValueAsString(updatePatientCommand))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }

    @Test
    public void updatePatientWithId_100_shouldReturnNotFound_whenPatientWithId_100_isNotInDatabase() throws Exception {
        Long patientId = 100L;
        UpdatePatientCommand updatePatientCommand =
                new UpdatePatientCommand(patientId, "1990620336382", "Angel Costin", 21, "Pascani", "0741222222", "patient@test.com");

        this.mockMvc.perform(put("/api/v1/patients")
                .content(mapper.writeValueAsString(updatePatientCommand))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void deletePatientWithId_1_shouldReturnNoContent_whenPatientWithId_1_isInDatabase() throws Exception {
        Long patientId = 1L;

        this.mockMvc.perform(delete("/api/v2/patients/{id}", patientId))
                .andExpect(status().isNoContent());
    }

    @Test
    public void deletePatientWithId_100_shouldReturnNotFound_whenPatientWithId_100_isNotInDatabase() throws Exception {
        Long patientId = 100L;

        this.mockMvc.perform(delete("/api/v2/patients/{id}", patientId))
                .andExpect(status().isNotFound());
    }
}
