package com.fii.patient_service.cqrs.commands;

import com.fii.patient_service.entities.Patient;
import com.fii.patient_service.exceptions.EntityNotFoundException;
import com.fii.patient_service.repositories.PatientRepository;
import com.fii.patient_service.resources.Constants;
import com.fii.patient_service.services.UserService;
import com.fii.patient_service.services.rabbitmq.PatientRabbitMQSender;
import io.jkratz.mediator.core.CommandHandler;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class RemovePatientCommandHandler implements CommandHandler<RemovePatientCommand> {
    private final PatientRepository patientRepository;
    private final UserService userService;
    private final PatientRabbitMQSender patientRabbitMQSender;

    @Override
    public void handle(@NotNull RemovePatientCommand removePatientCommand) {
        Patient patient = patientRepository.findById(removePatientCommand.getId())
                .orElseThrow(() -> new EntityNotFoundException(Constants.PATIENT_ENTITY, removePatientCommand.getId()));
        patientRepository.delete(patient);
        patientRabbitMQSender.sendRemovePatientMessage(removePatientCommand.getId());
        userService.removeByEmail(patient.getEmail());
    }
}
