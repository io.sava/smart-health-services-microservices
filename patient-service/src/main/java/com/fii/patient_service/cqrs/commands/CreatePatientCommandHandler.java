package com.fii.patient_service.cqrs.commands;

import com.fii.patient_service.entities.Patient;
import com.fii.patient_service.mappers.PatientMapper;
import com.fii.patient_service.repositories.PatientRepository;
import com.fii.patient_service.services.UserService;
import com.fii.patient_service.services.rabbitmq.PatientRabbitMQSender;
import io.jkratz.mediator.core.CommandHandler;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class CreatePatientCommandHandler implements CommandHandler<CreatePatientCommand> {
    private final PatientRepository patientRepository;
    private final PatientMapper patientMapper;
    private final UserService userService;
    private final PatientRabbitMQSender patientRabbitMQSender;

    @Override
    public void handle(@NotNull CreatePatientCommand createPatientCommand) {
        Patient patient = patientRepository.save(patientMapper.toPatient(createPatientCommand));
        patientRabbitMQSender.sendCreatePatientMessage(patient);
        userService.generateAccount(patient);
    }
}
