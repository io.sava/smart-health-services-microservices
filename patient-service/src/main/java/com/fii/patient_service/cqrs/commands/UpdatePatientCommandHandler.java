package com.fii.patient_service.cqrs.commands;

import com.fii.patient_service.entities.Patient;
import com.fii.patient_service.exceptions.EntityNotFoundException;
import com.fii.patient_service.mappers.PatientMapper;
import com.fii.patient_service.repositories.PatientRepository;
import com.fii.patient_service.resources.Constants;
import com.fii.patient_service.services.UserService;
import io.jkratz.mediator.core.CommandHandler;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class UpdatePatientCommandHandler implements CommandHandler<UpdatePatientCommand> {
    private final PatientRepository patientRepository;
    private final PatientMapper patientMapper;
    private final UserService userService;

    @Override
    public void handle(@NotNull UpdatePatientCommand updatePatientCommand) {
        checkIfExists(updatePatientCommand.getId());
        Patient patientToUpdate = patientMapper.toPatient(updatePatientCommand);
        patientToUpdate.setId(updatePatientCommand.getId());
        patientRepository.save(patientToUpdate);
        userService.updateEmail(updatePatientCommand.getId(), updatePatientCommand.getEmail());
    }

    private void checkIfExists(Long id) {
        if (patientRepository.findById(id).isEmpty()) {
            throw new EntityNotFoundException(Constants.PATIENT_ENTITY, id);
        }
    }
}
