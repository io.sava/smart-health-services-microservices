package com.fii.patient_service.cqrs.commands;

import io.jkratz.mediator.core.Command;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class RemovePatientCommand implements Command {
    private Long id;
}
