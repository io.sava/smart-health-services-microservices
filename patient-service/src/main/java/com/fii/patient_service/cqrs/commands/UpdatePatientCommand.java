package com.fii.patient_service.cqrs.commands;

import com.fii.patient_service.resources.Constants;
import com.fii.patient_service.resources.Messages;
import io.jkratz.mediator.core.Command;
import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
@AllArgsConstructor
public class UpdatePatientCommand implements Command {
    private Long id;

    @NotNull(message = Messages.CNP_CANNOT_BE_NULL)
    @Pattern(regexp = Constants.CNP_REGEX, message = Messages.INVALID_CNP_MESSAGE)
    private String cnp;

    @NotNull(message = Messages.NAME_CANNOT_BE_NULL)
    private String name;

    @NotNull(message = Messages.AGE_CANNOT_BE_NULL)
    @Min(value = 0, message = Messages.AGE_MUST_BE_POSITIVE_MESSAGE)
    private Integer age;

    private String address;
    private String phone;

    @Email
    @NotNull(message = Messages.EMAIL_CANNOT_BE_NULL)
    private String email;
}