package com.fii.patient_service.cqrs.queries;

import com.fii.patient_service.entities.Patient;
import io.jkratz.mediator.core.Request;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

@Data
@AllArgsConstructor
public class GetPatientsQuery implements Request<Page<Patient>> {
    private Pageable pageable;
}
