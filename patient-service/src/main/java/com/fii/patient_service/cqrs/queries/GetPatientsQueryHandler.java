package com.fii.patient_service.cqrs.queries;

import com.fii.patient_service.entities.Patient;
import com.fii.patient_service.repositories.PatientRepository;
import io.jkratz.mediator.core.RequestHandler;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class GetPatientsQueryHandler implements RequestHandler<GetPatientsQuery, Page<Patient>> {
    private final PatientRepository patientRepository;

    @Override
    public Page<Patient> handle(GetPatientsQuery getPatientsQuery) {
        return patientRepository.findAll(getPatientsQuery.getPageable());
    }
}
