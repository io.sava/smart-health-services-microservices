package com.fii.patient_service.cqrs.queries;

import com.fii.patient_service.entities.Patient;
import com.fii.patient_service.exceptions.EntityNotFoundException;
import com.fii.patient_service.repositories.PatientRepository;
import com.fii.patient_service.resources.Constants;
import io.jkratz.mediator.core.RequestHandler;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class GetPatientByIdQueryHandler implements RequestHandler<GetPatientByIdQuery, Patient> {
    private final PatientRepository patientRepository;

    @Override
    public Patient handle(GetPatientByIdQuery getPatientByIdQuery) {
        return patientRepository.findById(getPatientByIdQuery.getId())
                .orElseThrow(() -> new EntityNotFoundException(Constants.PATIENT_ENTITY, getPatientByIdQuery.getId()));
    }
}
