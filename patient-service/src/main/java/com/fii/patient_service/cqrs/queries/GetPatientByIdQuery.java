package com.fii.patient_service.cqrs.queries;

import com.fii.patient_service.entities.Patient;
import io.jkratz.mediator.core.Request;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class GetPatientByIdQuery implements Request<Patient> {
    private Long id;
}
