package com.fii.patient_service.mappers;

import com.fii.patient_service.cqrs.commands.CreatePatientCommand;
import com.fii.patient_service.cqrs.commands.UpdatePatientCommand;
import com.fii.patient_service.dtos.PatientDto;
import com.fii.patient_service.entities.Patient;
import org.springframework.stereotype.Component;

@Component
public final class PatientMapper {
    public Patient toPatient(PatientDto patientDto) {
        Patient patient = new Patient();
        patient.setCnp(patientDto.getCnp());
        patient.setName(patientDto.getName());
        patient.setAddress(patientDto.getAddress());
        patient.setAge(patientDto.getAge());
        patient.setPhone(patientDto.getPhone());
        patient.setEmail(patientDto.getEmail());

        return patient;
    }

    public Patient toPatient(CreatePatientCommand createPatientCommand) {
        Patient patient = new Patient();
        patient.setCnp(createPatientCommand.getCnp());
        patient.setName(createPatientCommand.getName());
        patient.setAddress(createPatientCommand.getAddress());
        patient.setAge(createPatientCommand.getAge());
        patient.setPhone(createPatientCommand.getPhone());
        patient.setEmail(createPatientCommand.getEmail());

        return patient;
    }

    public Patient toPatient(UpdatePatientCommand updatePatientCommand) {
        Patient patient = new Patient();
        patient.setCnp(updatePatientCommand.getCnp());
        patient.setName(updatePatientCommand.getName());
        patient.setAddress(updatePatientCommand.getAddress());
        patient.setAge(updatePatientCommand.getAge());
        patient.setPhone(updatePatientCommand.getPhone());
        patient.setEmail(updatePatientCommand.getEmail());

        return patient;
    }
}
