package com.fii.patient_service.services;

import com.fii.patient_service.dtos.AccountDetailsDto;
import com.fii.patient_service.dtos.UpdateAccountEmailDto;
import com.fii.patient_service.entities.Patient;
import com.fii.patient_service.services.rabbitmq.UserRabbitMQSender;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserService {
    private static final String PATIENT_ROLE = "PATIENT";
    private final UserRabbitMQSender userRabbitMqSender;

    public void generateAccount(Patient patient) {
        userRabbitMqSender.sendCreateAccountMessage(getAccountDetails(patient));
    }

    public void updateEmail(Long patientId, String email) {
        userRabbitMqSender.sendUpdateAccountEmailMessage(new UpdateAccountEmailDto(patientId, email, PATIENT_ROLE));
    }

    public void removeByEmail(String email) {
        userRabbitMqSender.sendRemoveAccountByEmailMessage(email);
    }

    private AccountDetailsDto getAccountDetails(Patient patient) {
        return new AccountDetailsDto(patient.getId(), patient.getEmail(), PATIENT_ROLE);
    }
}
