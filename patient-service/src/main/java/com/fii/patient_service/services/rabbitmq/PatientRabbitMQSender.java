package com.fii.patient_service.services.rabbitmq;

import com.fii.patient_service.entities.Patient;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PatientRabbitMQSender {
    private final RabbitTemplate rabbitTemplate;

    @Value("${spring.rabbitmq.template.patient-exchange}")
    private String exchange;

    @Value("${rabbitmq.routing-key.create-patient}")
    private String createPatientRoutingKey;

    @Value("${rabbitmq.routing-key.update-patient}")
    private String updatePatientRoutingKey;

    @Value("${rabbitmq.routing-key.remove-patient}")
    private String removePatientRoutingKey;

    public void sendCreatePatientMessage(Patient patient) {
        rabbitTemplate.convertAndSend(exchange, createPatientRoutingKey, patient);
    }

    public void sendUpdatePatientMessage(Patient patient) {
        rabbitTemplate.convertAndSend(exchange, updatePatientRoutingKey, patient);
    }

    public void sendRemovePatientMessage(Long patientId) {
        rabbitTemplate.convertAndSend(exchange, removePatientRoutingKey, patientId);
    }
}
