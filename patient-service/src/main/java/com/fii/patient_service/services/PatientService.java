package com.fii.patient_service.services;

import com.fii.patient_service.dtos.PatientDto;
import com.fii.patient_service.entities.Patient;
import com.fii.patient_service.exceptions.EntityNotFoundException;
import com.fii.patient_service.mappers.PatientMapper;
import com.fii.patient_service.repositories.PatientRepository;
import com.fii.patient_service.resources.Constants;
import com.fii.patient_service.services.rabbitmq.PatientRabbitMQSender;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
@RequiredArgsConstructor
public class PatientService {
    private final PatientRepository patientRepository;
    private final PatientMapper patientMapper;
    private final UserService userService;
    private final PatientRabbitMQSender patientRabbitMQSender;

    public Page<Patient> getAll(Pageable pageable) {
        return patientRepository.findAll(pageable);
    }

    public Patient getById(Long id) {
        return patientRepository.findById(id).orElseThrow(() -> new EntityNotFoundException(Constants.PATIENT_ENTITY, id));
    }

    public void create(PatientDto patientDto) {
        Patient patient = patientRepository.save(patientMapper.toPatient(patientDto));
        patientRabbitMQSender.sendCreatePatientMessage(patient);
        userService.generateAccount(patient);
    }

    public void update(Long id, PatientDto patientDto) {
        checkIfExists(id);
        Patient patientToUpdate = patientMapper.toPatient(patientDto);
        patientToUpdate.setId(id);
        patientRepository.save(patientToUpdate);
        userService.updateEmail(id, patientDto.getEmail());
    }

    public void remove(Long id) {
        Patient patient = getById(id);
        patientRepository.delete(patient);
        patientRabbitMQSender.sendRemovePatientMessage(id);
        userService.removeByEmail(patient.getEmail());
    }

    private void checkIfExists(Long id) {
        if (patientRepository.findById(id).isEmpty()) {
            throw new EntityNotFoundException(Constants.PATIENT_ENTITY, id);
        }
    }
}
