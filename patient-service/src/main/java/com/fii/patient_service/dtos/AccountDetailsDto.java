package com.fii.patient_service.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
public class AccountDetailsDto {
    @NotNull(message = "OwnerId cannot be null")
    private Long ownerId;

    @Email
    @NotNull(message = "Email cannot be null")
    private String email;

    @NotNull(message = "Role cannot be null")
    private String role;
}
