package com.fii.patient_service.resources;

public class Constants {
    public static final String PATIENT_ENTITY = "Patient";
    public static final String CNP_REGEX = "\\b[1-8]\\d{2}(0[1-9]|1[0-2])(0[1-9]|[12]\\d|3[01])(0[1-9]|[1-4]\\d|5[0-2]|99)\\d{4}\\b";
}
