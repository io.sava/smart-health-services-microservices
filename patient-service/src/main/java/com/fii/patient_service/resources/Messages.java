package com.fii.patient_service.resources;

public class Messages {
    public static final String INVALID_CNP_MESSAGE = "Invalid CNP";
    public static final String AGE_MUST_BE_POSITIVE_MESSAGE = "The age must be positive";
    public static final String CNP_CANNOT_BE_NULL = "Cnp cannot be null";
    public static final String AGE_CANNOT_BE_NULL = "Age cannot be null";
    public static final String NAME_CANNOT_BE_NULL = "Name cannot be null";
    public static final String EMAIL_CANNOT_BE_NULL = "Email cannot be null";
}
