package com.fii.patient_service.controllers.v1;

import com.fii.patient_service.dtos.PatientDto;
import com.fii.patient_service.entities.Patient;
import com.fii.patient_service.services.PatientService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.Min;

@RestController
@RequestMapping("api/v1/patients")
@RequiredArgsConstructor
public class PatientControllerV1 {
    private final PatientService patientService;

    @GetMapping
    public Page<Patient> getAll(Pageable pageable) {
        return patientService.getAll(pageable);
    }

    @GetMapping("/{id}")
    public Patient getById(@PathVariable @Valid @Min(0) Long id) {
        return patientService.getById(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void create(@RequestBody @Valid PatientDto patientDto) {
        patientService.create(patientDto);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void update(@PathVariable @Valid @Min(0) Long id, @RequestBody @Valid PatientDto patientDto) {
        patientService.update(id, patientDto);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remove(@PathVariable @Valid @Min(0) Long id) {
        patientService.remove(id);
    }
}