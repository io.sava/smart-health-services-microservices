package com.fii.patient_service.controllers.v2;

import com.fii.patient_service.cqrs.commands.CreatePatientCommand;
import com.fii.patient_service.cqrs.commands.RemovePatientCommand;
import com.fii.patient_service.cqrs.commands.UpdatePatientCommand;
import com.fii.patient_service.cqrs.queries.GetPatientByIdQuery;
import com.fii.patient_service.cqrs.queries.GetPatientsQuery;
import com.fii.patient_service.entities.Patient;
import io.jkratz.mediator.core.Mediator;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.Min;

@RestController
@RequestMapping("api/v2/patients")
@RequiredArgsConstructor
public class PatientControllerV2 {
    private final Mediator mediator;

    @GetMapping
    public Page<Patient> getAll(Pageable pageable) {
        return mediator.dispatch(new GetPatientsQuery(pageable));
    }

    @GetMapping("/{id}")
    public Patient getById(@PathVariable @Valid @Min(0) Long id) {
        return mediator.dispatch(new GetPatientByIdQuery(id));
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void create(@RequestBody @Valid CreatePatientCommand createPatientCommand) {
        mediator.dispatch(createPatientCommand);
    }

    @PutMapping
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void update(@RequestBody @Valid UpdatePatientCommand updatePatientCommand) {
        mediator.dispatch(updatePatientCommand);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remove(@PathVariable @Valid @Min(0) Long id) {
        mediator.dispatch(new RemovePatientCommand(id));
    }
}
