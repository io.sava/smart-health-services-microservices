package com.fii.hospital_management_service.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fii.hospital_management_service.dtos.HospitalDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
@RunWith(SpringRunner.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class HospitalControllerIT {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper mapper;

    @Test
    public void getHospitalWithId_1_shouldReturnOk_whenHospitalWithId_1_isInDatabase() throws Exception {
        Long hospitalId = 1L;

        this.mockMvc.perform(get("/api/v1/hospitals/{id}", hospitalId))
                .andExpect(status().isOk());
    }

    @Test
    public void getHospitalWithId_100_shouldReturnNotFound_whenHospitalWithId_100_isNotInDatabase() throws Exception {
        Long hospitalId = 100L;

        this.mockMvc.perform(get("/api/v1/hospitals/{id}", hospitalId))
                .andExpect(status().isNotFound());
    }

    @Test
    public void createHospital_shouldReturnCreated() throws Exception {
        HospitalDto hospital =
                new HospitalDto("Spital Test", "Adresa spital test", "Fax test", "Phone test", "email@test.com");

        this.mockMvc.perform(post("/api/v1/hospitals")
                .content(mapper.writeValueAsString(hospital))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    @Test
    public void updateHospitalWithId_1_shouldReturnNoContent_whenHospitalWithId_1_isInDatabase() throws Exception {
        Long hospitalId = 1L;
        HospitalDto hospital =
                new HospitalDto("Nume nou de spital", "Adresa spital test", "Fax test", "Phone test", "email@test.com");

        this.mockMvc.perform(put("/api/v1/hospitals/{id}", hospitalId)
                .content(mapper.writeValueAsString(hospital))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }

    @Test
    public void updateHospitalWithId_100_shouldReturnNotFound_whenHospitalWithId_100_isNotInDatabase() throws Exception {
        Long hospitalId = 100L;
        HospitalDto hospital =
                new HospitalDto("Nume nou de spital", "Adresa spital test", "Fax test", "Phone test", "email@test.com");

        this.mockMvc.perform(put("/api/v1/hospitals/{id}", hospitalId)
                .content(mapper.writeValueAsString(hospital))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void deleteHospitalWithId_1_shouldReturnNoContent_whenHospitalWithId_1_isInDatabase() throws Exception {
        Long hospitalId = 1L;

        this.mockMvc.perform(delete("/api/v1/hospitals/{id}", hospitalId))
                .andExpect(status().isNoContent());
    }

    @Test
    public void deleteHospitalWithId_100_shouldReturnNotFound_whenHospitalWithId_100_isNotInDatabase() throws Exception {
        Long hospitalId = 100L;

        this.mockMvc.perform(delete("/api/v1/hospitals/{id}", hospitalId))
                .andExpect(status().isNotFound());
    }
}
