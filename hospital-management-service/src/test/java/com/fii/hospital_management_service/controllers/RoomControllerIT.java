package com.fii.hospital_management_service.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fii.hospital_management_service.dtos.RoomDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
@RunWith(SpringRunner.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class RoomControllerIT {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper mapper;

    @Test
    public void getRoomsByHospital_shouldReturnOk_whenHospitalWithId_1_isInDatabase() throws Exception {
        Long hospitalId = 1L;

        this.mockMvc.perform(get("/api/v1/rooms/hospitals/{id}", hospitalId))
                .andExpect(status().isOk());
    }

    @Test
    public void getRoomsByHospital_shouldReturnNotFound_whenHospitalWithId_100_isNotInDatabase() throws Exception {
        Long hospitalId = 100L;

        this.mockMvc.perform(get("/api/v1/rooms/hospitals/{id}", hospitalId))
                .andExpect(status().isNotFound());
    }

    @Test
    public void createRoom_shouldReturnCreated() throws Exception {
        RoomDto room =
                new RoomDto("ATI", 1, 1L);

        this.mockMvc.perform(post("/api/v1/rooms")
                .content(mapper.writeValueAsString(room))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    @Test
    public void updateRoomWithId_1_shouldReturnNoContent_whenRoomWithId_1_isInDatabase() throws Exception {
        Long roomId = 1L;
        RoomDto room =
                new RoomDto("ATI", 1, 1L);

        this.mockMvc.perform(put("/api/v1/rooms/{id}", roomId)
                .content(mapper.writeValueAsString(room))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }

    @Test
    public void updateRoomWithId_1_shouldReturnNotFound_whenHospitalWithId_100_isNotInDatabase() throws Exception {
        Long roomId = 1L;
        RoomDto room =
                new RoomDto("ATI", 1, 100L);

        this.mockMvc.perform(put("/api/v1/rooms/{id}", roomId)
                .content(mapper.writeValueAsString(room))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void updateRoomWithId_100_shouldReturnNotFound_whenRoomWithId_100_isNotInDatabase() throws Exception {
        Long roomId = 100L;
        RoomDto room =
                new RoomDto("ATI", 1, 1L);

        this.mockMvc.perform(put("/api/v1/rooms/{id}", roomId)
                .content(mapper.writeValueAsString(room))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void deleteRoomWithId_1_shouldReturnNoContent_whenRoomWithId_1_isInDatabase() throws Exception {
        Long roomId = 1L;

        this.mockMvc.perform(delete("/api/v1/rooms/{id}", roomId))
                .andExpect(status().isNoContent());
    }

    @Test
    public void deleteRoomWithId_100_shouldReturnNotFound_whenRoomWithId_100_isNotInDatabase() throws Exception {
        Long roomId = 100L;

        this.mockMvc.perform(delete("/api/v1/rooms/{id}", roomId))
                .andExpect(status().isNotFound());
    }
}
