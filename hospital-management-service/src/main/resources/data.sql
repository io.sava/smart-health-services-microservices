-- hospitals
insert into hospitals(name, address, fax, email, phone)
values ('Spitalul Clinic Judetean de Urgenta Sf. Spiridon Iasi', 'Bd. Independenței nr. 1, cod 700111, IAȘI',
        '0232-217781', 'contact@spitalspiridon.ro', '0232-240822');
insert into hospitals(name, address, fax, email, phone)
values ('Spitalul Clinic de Boli Infectioase Sfanta Parascheva Iasi', 'Str. Octav Botez nr. 2, Iasi, Iasi',
        '0232 264 252', 'sbc_iasi@mail.dntis.ro', '0232 267 719');
insert into hospitals(name, address, fax, email, phone)
values ('Spitalul Clinic de Urgenta Militar Dr. Iacob Czihac Iasi', 'Str. Berthelot Henri Mathias nr. 7-9, Iasi, Iasi',
        '0232 216 844', 'smuis32@yahoo.com', ' 0733 981 480');

-- departments
insert into departments(name, hospital_id)
values ('Medicina interna', 1);
insert into departments(name, hospital_id)
values ('Chirurgie generala', 1);
insert into departments(name, hospital_id)
values ('Ortopedie si traumatologie', 1);
insert into departments(name, hospital_id)
values ('Oftalmologie', 1);
insert into departments(name, hospital_id)
values ('ORL', 1);
insert into departments(name, hospital_id)
values ('Dermato-Venerologie', 1);

insert into departments(name, hospital_id)
values ('Medicina interna', 2);
insert into departments(name, hospital_id)
values ('Chirurgie generala', 2);
insert into departments(name, hospital_id)
values ('Ortopedie si traumatologie', 2);
insert into departments(name, hospital_id)
values ('Oftalmologie', 2);
insert into departments(name, hospital_id)
values ('ORL', 2);
insert into departments(name, hospital_id)
values ('Dermato-Venerologie', 2);

insert into departments(name, hospital_id)
values ('Medicina interna', 3);
insert into departments(name, hospital_id)
values ('Chirurgie generala', 3);
insert into departments(name, hospital_id)
values ('Ortopedie si traumatologie', 3);
insert into departments(name, hospital_id)
values ('Oftalmologie', 3);
insert into departments(name, hospital_id)
values ('ORL', 3);
insert into departments(name, hospital_id)
values ('Dermato-Venerologie', 3);

-- rooms
insert into rooms(type, floor, hospital_id)
values ('ATI', 1, 1);
insert into rooms(type, floor, hospital_id)
values ('Salon', 1, 1);
insert into rooms(type, floor, hospital_id)
values ('Salon', 2, 1);

insert into rooms(type, floor, hospital_id)
values ('ATI', 2, 2);
insert into rooms(type, floor, hospital_id)
values ('Salon', 2, 2);
insert into rooms(type, floor, hospital_id)
values ('Salon', 1, 2);

insert into rooms(type, floor, hospital_id)
values ('ATI', 2, 3);
insert into rooms(type, floor, hospital_id)
values ('Salon', 2, 3);
insert into rooms(type, floor, hospital_id)
values ('Salon', 1, 3);