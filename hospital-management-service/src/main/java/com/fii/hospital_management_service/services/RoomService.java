package com.fii.hospital_management_service.services;

import com.fii.hospital_management_service.dtos.RoomDto;
import com.fii.hospital_management_service.entities.Room;
import com.fii.hospital_management_service.exceptions.EntityNotFoundException;
import com.fii.hospital_management_service.mappers.RoomMapper;
import com.fii.hospital_management_service.repositories.HospitalRepository;
import com.fii.hospital_management_service.repositories.RoomRepository;
import com.fii.hospital_management_service.services.rabbitmq.RoomRabbitMQSender;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
@RequiredArgsConstructor
public class RoomService {
    private final RoomRepository roomRepository;
    private final HospitalRepository hospitalRepository;
    private final RoomMapper roomMapper;
    private final RoomRabbitMQSender roomRabbitMQSender;

    public Room getById(Long id) {
        return roomRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Room", id));
    }

    public List<Room> getByHospitalId(Long hospitalId) {
        if (hospitalRepository.findById(hospitalId).isEmpty()) {
            throw new EntityNotFoundException("Hospital", hospitalId);
        }

        return roomRepository.findAll().stream()
                .filter(room -> room.getHospital().getId().equals(hospitalId))
                .collect(Collectors.toList());
    }

    public void create(RoomDto roomDto) {
        Room room = roomRepository.save(roomMapper.toRoom(roomDto));
        roomRabbitMQSender.sendCreateRoomMessage(room.getId(), room.getHospital().getId());
    }

    public void update(Long id, RoomDto roomDto) {
        this.getById(id);
        Room roomToUpdate = roomMapper.toRoom(roomDto);
        roomToUpdate.setId(id);
        roomRepository.save(roomToUpdate);
        roomRabbitMQSender.sendUpdateRoomMessage(id, roomDto.getHospitalId());
    }

    public void remove(Long id) {
        Room room = this.getById(id);
        roomRepository.delete(room);
        roomRabbitMQSender.sendRemoveRoomMessage(id);
    }
}
