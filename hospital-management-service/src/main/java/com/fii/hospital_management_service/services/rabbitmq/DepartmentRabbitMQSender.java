package com.fii.hospital_management_service.services.rabbitmq;

import com.fii.hospital_management_service.dtos.rabbitmq.DepartmentDto;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class DepartmentRabbitMQSender {
    private final RabbitTemplate rabbitTemplate;

    @Value("${spring.rabbitmq.template.department-exchange}")
    private String exchange;

    @Value("${rabbitmq.routing-key.create-department}")
    private String createDepartmentRoutingKey;

    @Value("${rabbitmq.routing-key.update-department}")
    private String updateDepartmentRoutingKey;

    @Value("${rabbitmq.routing-key.remove-department}")
    private String removeDepartmentRoutingKey;

    public void sendCreateDepartmentMessage(Long departmentId, Long hospitalId) {
        rabbitTemplate.convertAndSend(exchange, createDepartmentRoutingKey, new DepartmentDto(departmentId, hospitalId));
    }

    public void sendUpdateDepartmentMessage(Long departmentId, Long hospitalId) {
        rabbitTemplate.convertAndSend(exchange, updateDepartmentRoutingKey, new DepartmentDto(departmentId, hospitalId));
    }

    public void sendRemoveDepartmentMessage(Long departmentId) {
        rabbitTemplate.convertAndSend(exchange, removeDepartmentRoutingKey, departmentId);
    }
}
