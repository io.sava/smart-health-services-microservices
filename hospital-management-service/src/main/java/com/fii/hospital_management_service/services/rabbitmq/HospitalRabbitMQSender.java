package com.fii.hospital_management_service.services.rabbitmq;

import com.fii.hospital_management_service.entities.Hospital;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class HospitalRabbitMQSender {
    private final RabbitTemplate rabbitTemplate;

    @Value("${spring.rabbitmq.template.hospital-exchange}")
    private String exchange;

    @Value("${rabbitmq.routing-key.create-hospital}")
    private String createHospitalRoutingKey;

    @Value("${rabbitmq.routing-key.update-hospital}")
    private String updateHospitalRoutingKey;

    @Value("${rabbitmq.routing-key.remove-hospital}")
    private String removeHospitalRoutingKey;

    public void sendCreateHospitalMessage(Hospital hospital) {
        rabbitTemplate.convertAndSend(exchange, createHospitalRoutingKey, hospital);
    }

    public void sendUpdateHospitalMessage(Hospital hospital) {
        rabbitTemplate.convertAndSend(exchange, updateHospitalRoutingKey, hospital);
    }

    public void sendRemoveHospitalMessage(Long hospitalId) {
        rabbitTemplate.convertAndSend(exchange, removeHospitalRoutingKey, hospitalId);
    }
}
