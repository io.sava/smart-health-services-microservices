package com.fii.hospital_management_service.services;

import com.fii.hospital_management_service.dtos.DepartmentDto;
import com.fii.hospital_management_service.entities.Department;
import com.fii.hospital_management_service.exceptions.EntityNotFoundException;
import com.fii.hospital_management_service.mappers.DepartmentMapper;
import com.fii.hospital_management_service.repositories.DepartmentRepository;
import com.fii.hospital_management_service.repositories.HospitalRepository;
import com.fii.hospital_management_service.services.rabbitmq.DepartmentRabbitMQSender;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
@RequiredArgsConstructor
public class DepartmentService {
    private static final String HOSPITAL_ENTITY = "Hospital";
    private final DepartmentRepository departmentRepository;
    private final HospitalRepository hospitalRepository;
    private final DepartmentMapper departmentMapper;
    private final DepartmentRabbitMQSender departmentRabbitMQSender;

    public Department getById(Long id) {
        return departmentRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Department", id));
    }

    public List<Department> getByHospitalId(Long hospitalId) {
        if (hospitalRepository.findById(hospitalId).isEmpty()) {
            throw new EntityNotFoundException(HOSPITAL_ENTITY, hospitalId);
        }

        return departmentRepository.findAll().stream()
                .filter(department -> department.getHospital().getId().equals(hospitalId))
                .collect(Collectors.toList());
    }

    public void create(DepartmentDto departmentDto) {
        if (hospitalRepository.findById(departmentDto.getHospitalId()).isEmpty()) {
            throw new EntityNotFoundException(HOSPITAL_ENTITY, departmentDto.getHospitalId());
        }
        Long departmentId = departmentRepository.save(departmentMapper.toDepartment(departmentDto)).getId();
        departmentRabbitMQSender.sendCreateDepartmentMessage(departmentId, departmentDto.getHospitalId());
    }

    public void update(Long id, DepartmentDto departmentDto) {
        this.getById(id);
        if (hospitalRepository.findById(departmentDto.getHospitalId()).isEmpty()) {
            throw new EntityNotFoundException(HOSPITAL_ENTITY, departmentDto.getHospitalId());
        }
        Department departmentToUpdate = departmentMapper.toDepartment(departmentDto);
        departmentToUpdate.setId(id);
        departmentRepository.save(departmentToUpdate);
        departmentRabbitMQSender.sendUpdateDepartmentMessage(id, departmentDto.getHospitalId());
    }

    public void remove(Long id) {
        Department department = this.getById(id);
        departmentRepository.delete(department);
        departmentRabbitMQSender.sendRemoveDepartmentMessage(id);
    }
}
