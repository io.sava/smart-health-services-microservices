package com.fii.hospital_management_service.services;

import com.fii.hospital_management_service.dtos.HospitalDto;
import com.fii.hospital_management_service.entities.Hospital;
import com.fii.hospital_management_service.exceptions.EntityNotFoundException;
import com.fii.hospital_management_service.mappers.HospitalMapper;
import com.fii.hospital_management_service.repositories.HospitalRepository;
import com.fii.hospital_management_service.services.rabbitmq.HospitalRabbitMQSender;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class HospitalService {
    private final HospitalRepository hospitalRepository;
    private final HospitalMapper hospitalMapper;
    private final DepartmentService departmentService;
    private final RoomService roomService;
    private final HospitalRabbitMQSender hospitalRabbitMQSender;

    public List<Hospital> getAll() {
        return new ArrayList<>(hospitalRepository.findAll());
    }

    public Hospital getById(Long id) {
        return hospitalRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Hospital", id));
    }

    public void create(HospitalDto hospitalDto) {
        Hospital hospital = hospitalRepository.save(hospitalMapper.toHospital(hospitalDto));
        hospitalRabbitMQSender.sendCreateHospitalMessage(hospital);
    }

    public void update(Long id, HospitalDto hospitalDto) {
        this.getById(id);
        Hospital hospitalToUpdate = hospitalMapper.toHospital(hospitalDto);
        hospitalToUpdate.setId(id);
        Hospital hospital = hospitalRepository.save(hospitalToUpdate);
        hospitalRabbitMQSender.sendUpdateHospitalMessage(hospital);
    }

    public void remove(Long id) {
        Hospital hospital = this.getById(id);
        hospital.getDepartments().forEach(department -> departmentService.remove(department.getId()));
        hospital.getRooms().forEach(room -> roomService.remove(room.getId()));
        hospitalRepository.delete(hospital);
        hospitalRabbitMQSender.sendRemoveHospitalMessage(id);
    }
}
