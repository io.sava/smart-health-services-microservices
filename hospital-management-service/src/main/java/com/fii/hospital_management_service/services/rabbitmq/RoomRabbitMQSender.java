package com.fii.hospital_management_service.services.rabbitmq;

import com.fii.hospital_management_service.dtos.rabbitmq.RoomDto;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class RoomRabbitMQSender {
    private final RabbitTemplate rabbitTemplate;

    @Value("${spring.rabbitmq.template.room-exchange}")
    private String exchange;

    @Value("${rabbitmq.routing-key.create-room}")
    private String createRoomRoutingKey;

    @Value("${rabbitmq.routing-key.update-room}")
    private String updateRoomRoutingKey;

    @Value("${rabbitmq.routing-key.remove-room}")
    private String removeRoomRoutingKey;

    public void sendCreateRoomMessage(Long roomId, Long hospitalId) {
        rabbitTemplate.convertAndSend(exchange, createRoomRoutingKey, new RoomDto(roomId, hospitalId));
    }

    public void sendUpdateRoomMessage(Long roomId, Long hospitalId) {
        rabbitTemplate.convertAndSend(exchange, updateRoomRoutingKey, new RoomDto(roomId, hospitalId));
    }

    public void sendRemoveRoomMessage(Long roomId) {
        rabbitTemplate.convertAndSend(exchange, removeRoomRoutingKey, roomId);
    }
}
