package com.fii.hospital_management_service.repositories;

import com.fii.hospital_management_service.entities.Department;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DepartmentRepository extends JpaRepository<Department, Long> {
}
