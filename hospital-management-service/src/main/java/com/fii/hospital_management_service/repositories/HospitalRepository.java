package com.fii.hospital_management_service.repositories;

import com.fii.hospital_management_service.entities.Hospital;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HospitalRepository extends JpaRepository<Hospital, Long> {
}
