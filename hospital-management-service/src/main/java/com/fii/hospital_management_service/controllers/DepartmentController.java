package com.fii.hospital_management_service.controllers;

import com.fii.hospital_management_service.dtos.DepartmentDto;
import com.fii.hospital_management_service.entities.Department;
import com.fii.hospital_management_service.services.DepartmentService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.util.List;

@RestController
@RequestMapping("api/v1/departments")
@RequiredArgsConstructor
public class DepartmentController {
    private final DepartmentService departmentService;

    @GetMapping("/{id}")
    public Department getById(@PathVariable @Valid @Min(0) Long id) {
        return departmentService.getById(id);
    }

    @GetMapping("/hospitals/{hospitalId}")
    public List<Department> getByHospitalId(@PathVariable @Valid @Min(0) Long hospitalId) {
        return departmentService.getByHospitalId(hospitalId);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void create(@RequestBody @Valid DepartmentDto departmentDto) {
        departmentService.create(departmentDto);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void update(@PathVariable @Valid @Min(0) Long id, @RequestBody @Valid DepartmentDto departmentDto) {
        departmentService.update(id, departmentDto);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remove(@PathVariable @Valid @Min(0) Long id) {
        departmentService.remove(id);
    }
}