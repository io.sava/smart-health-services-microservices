package com.fii.hospital_management_service.controllers;

import com.fii.hospital_management_service.dtos.RoomDto;
import com.fii.hospital_management_service.entities.Room;
import com.fii.hospital_management_service.services.RoomService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.util.List;

@RestController
@RequestMapping("api/v1/rooms")
@RequiredArgsConstructor
public class RoomController {
    private final RoomService roomService;

    @GetMapping("/{id}")
    public Room getById(@PathVariable @Valid @Min(0) Long id) {
        return roomService.getById(id);
    }

    @GetMapping("/hospitals/{hospitalId}")
    public List<Room> getByHospitalId(@PathVariable @Valid @Min(0) Long hospitalId) {
        return roomService.getByHospitalId(hospitalId);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void create(@RequestBody @Valid RoomDto roomDto) {
        roomService.create(roomDto);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void update(@PathVariable @Valid @Min(0) Long id, @RequestBody @Valid RoomDto roomDto) {
        roomService.update(id, roomDto);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remove(@PathVariable @Valid @Min(0) Long id) {
        roomService.remove(id);
    }
}