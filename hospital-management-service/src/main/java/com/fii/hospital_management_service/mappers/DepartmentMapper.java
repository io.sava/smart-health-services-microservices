package com.fii.hospital_management_service.mappers;

import com.fii.hospital_management_service.dtos.DepartmentDto;
import com.fii.hospital_management_service.entities.Department;
import com.fii.hospital_management_service.exceptions.EntityNotFoundException;
import com.fii.hospital_management_service.repositories.HospitalRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public final class DepartmentMapper {
    private final HospitalRepository hospitalRepository;

    public Department toDepartment(DepartmentDto departmentDto) {
        Department department = new Department();
        department.setName(departmentDto.getName());
        department.setHospital(hospitalRepository.findById(departmentDto.getHospitalId())
                .orElseThrow(() -> new EntityNotFoundException("Hospital", departmentDto.getHospitalId())));

        return department;
    }
}
