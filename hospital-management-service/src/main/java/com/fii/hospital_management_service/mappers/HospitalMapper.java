package com.fii.hospital_management_service.mappers;

import com.fii.hospital_management_service.dtos.HospitalDto;
import com.fii.hospital_management_service.entities.Hospital;
import org.springframework.stereotype.Component;

@Component
public final class HospitalMapper {
    public Hospital toHospital(HospitalDto hospitalDto) {
        Hospital hospital = new Hospital();
        hospital.setName(hospitalDto.getName());
        hospital.setAddress(hospitalDto.getAddress());
        hospital.setFax(hospitalDto.getFax());
        hospital.setEmail(hospitalDto.getEmail());
        hospital.setPhone(hospitalDto.getPhone());

        return hospital;
    }
}
