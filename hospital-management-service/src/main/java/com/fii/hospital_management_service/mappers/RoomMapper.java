package com.fii.hospital_management_service.mappers;

import com.fii.hospital_management_service.dtos.RoomDto;
import com.fii.hospital_management_service.entities.Room;
import com.fii.hospital_management_service.exceptions.EntityNotFoundException;
import com.fii.hospital_management_service.repositories.HospitalRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public final class RoomMapper {
    private final HospitalRepository hospitalRepository;

    public Room toRoom(RoomDto roomDto) {
        Room room = new Room();
        room.setFloor(roomDto.getFloor());
        room.setType(roomDto.getType());
        room.setHospital(hospitalRepository.findById(roomDto.getHospitalId())
                .orElseThrow(() -> new EntityNotFoundException("Hospital", roomDto.getHospitalId())));

        return room;
    }
}