package com.fii.hospital_management_service.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

@EqualsAndHashCode(callSuper = false)
@Data
@AllArgsConstructor
@Builder
@Entity
@Table(name = "hospitals")
public class Hospital extends BaseEntity {
    private String name;
    private String address;
    private String fax;
    private String email;
    private String phone;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "hospital")
    private List<Department> departments;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "hospital")
    private List<Room> rooms;

    public Hospital() {
        this.departments = new ArrayList<>();
        this.rooms = new ArrayList<>();
    }
}
