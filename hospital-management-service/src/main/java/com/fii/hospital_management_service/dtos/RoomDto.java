package com.fii.hospital_management_service.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RoomDto {
    @NotNull(message = "Type cannot be null")
    private String type;

    @NotNull(message = "Floor cannot be null")
    private Integer floor;

    @NotNull(message = "HospitalId cannot be null")
    private Long hospitalId;
}
