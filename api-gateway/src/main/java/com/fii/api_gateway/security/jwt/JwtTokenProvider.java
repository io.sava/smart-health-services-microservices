package com.fii.api_gateway.security.jwt;

import com.fii.api_gateway.constants.ExceptionsConstants;
import com.fii.api_gateway.entities.Role;
import com.fii.api_gateway.exceptions.CustomException;
import com.fii.api_gateway.security.UserDetailsServiceImpl;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import javax.annotation.PostConstruct;
import javax.ws.rs.core.HttpHeaders;
import java.util.Base64;
import java.util.Collections;
import java.util.LinkedHashMap;

@Component
public class JwtTokenProvider {
    private static final String HEADER_PREFIX = "Bearer ";
    private static final String AUTH_FIELD = "auth";
    private static final String AUTHORITY_FIELD = "authority";

    private final UserDetailsServiceImpl userDetailsService;

    @Value("${security.jwt.token.secret-key}")
    private String secretKey;

    public JwtTokenProvider(UserDetailsServiceImpl userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    @PostConstruct
    protected void init() {
        secretKey = Base64.getEncoder().encodeToString(secretKey.getBytes());
    }

    public Authentication getAuthentication(String token) {
        Mono<UserDetails> userDetails = userDetailsService.findByUsername(getUsername(token));
        return new UsernamePasswordAuthenticationToken(userDetails, "", Collections.singletonList(getRole(token)));
    }

    public String getUsername(String token) {
        return Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody().getSubject();
    }

    private Role getRole(String token) {
        return Role.valueOf((String) ((LinkedHashMap)
                Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody().get(AUTH_FIELD)).get(AUTHORITY_FIELD));
    }

    public String resolveToken(ServerHttpRequest request) {
        String bearerToken = request.getHeaders().getFirst(HttpHeaders.AUTHORIZATION);
        if (bearerToken != null && bearerToken.startsWith(HEADER_PREFIX)) {
            return bearerToken.substring(7);
        }
        return null;
    }

    public boolean validateToken(String token) {
        try {
            Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token);
            return true;
        } catch (JwtException | IllegalArgumentException exception) {
            throw new CustomException(ExceptionsConstants.INVALID_TOKEN_MESSAGE, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}