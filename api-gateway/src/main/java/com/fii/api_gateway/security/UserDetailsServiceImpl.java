package com.fii.api_gateway.security;

import com.fii.api_gateway.constants.ApiConstants;
import com.fii.api_gateway.dtos.UserDto;
import com.fii.api_gateway.mappers.UserMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.ReactiveUserDetailsService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Service
@RequiredArgsConstructor
public class UserDetailsServiceImpl implements ReactiveUserDetailsService {
    private final UserMapper userMapper;
    private final WebClient.Builder webClient;

    @Override
    public Mono<UserDetails> findByUsername(String email) {
        return webClient.build()
                .get()
                .uri(ApiConstants.USERS_MANAGEMENT_SERVICE_URL + email)
                .retrieve()
                .bodyToMono(UserDto.class)
                .map(userMapper::toUser);
    }
}
