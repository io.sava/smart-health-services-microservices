package com.fii.api_gateway.dtos;

import lombok.Data;

@Data
public class UserDto {
    private String email;
    private String password;
    private String role;
    private Long ownerId;
}
