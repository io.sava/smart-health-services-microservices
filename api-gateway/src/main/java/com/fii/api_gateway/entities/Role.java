package com.fii.api_gateway.entities;

import org.springframework.security.core.GrantedAuthority;

public enum Role implements GrantedAuthority {
    ADMIN, DOCTOR, PATIENT, NURSE;

    public String getAuthority() {
        return name();
    }
}
