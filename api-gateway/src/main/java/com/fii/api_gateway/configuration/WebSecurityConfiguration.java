package com.fii.api_gateway.configuration;

import com.fii.api_gateway.constants.MicroservicesConstants;
import com.fii.api_gateway.entities.Role;
import com.fii.api_gateway.security.jwt.JwtTokenFilter;
import com.fii.api_gateway.security.jwt.JwtTokenProvider;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableReactiveMethodSecurity;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.SecurityWebFiltersOrder;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.server.SecurityWebFilterChain;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.reactive.CorsConfigurationSource;
import org.springframework.web.cors.reactive.UrlBasedCorsConfigurationSource;

import java.util.List;

@Configuration
@EnableWebFluxSecurity
@EnableReactiveMethodSecurity
@RequiredArgsConstructor
public class WebSecurityConfiguration {
    private final JwtTokenProvider jwtTokenProvider;
    @Value("${front-end.domain}")
    private String frontEndDomain;

    @Bean
    public SecurityWebFilterChain securityWebFilterChain(ServerHttpSecurity http) {
        return http.authorizeExchange()
                .pathMatchers(HttpMethod.GET, String.format("/%s/api/v1/departments", MicroservicesConstants.HOSPITAL_MANAGEMENT_SERVICE)).permitAll()
                .pathMatchers(HttpMethod.GET, String.format("/%s/api/v1/departments/**", MicroservicesConstants.HOSPITAL_MANAGEMENT_SERVICE)).permitAll()
                .pathMatchers(HttpMethod.GET, String.format("/%s/api/v1/hospitals", MicroservicesConstants.HOSPITAL_MANAGEMENT_SERVICE)).permitAll()
                .pathMatchers(HttpMethod.GET, String.format("/%s/api/v1/hospitals/**", MicroservicesConstants.HOSPITAL_MANAGEMENT_SERVICE)).permitAll()
                .pathMatchers(HttpMethod.GET, String.format("/%s/api/v1/rooms/**", MicroservicesConstants.HOSPITAL_MANAGEMENT_SERVICE)).permitAll()

                .pathMatchers(HttpMethod.POST, String.format("/%s/api/v1/users/**", MicroservicesConstants.USER_MANAGEMENT_SERVICE)).permitAll()

                .pathMatchers(HttpMethod.GET, String.format("/%s/api/v1/patients", MicroservicesConstants.PATIENT_SERVICE)).hasAnyAuthority(Role.DOCTOR.getAuthority(), Role.NURSE.getAuthority(), Role.ADMIN.getAuthority())
                .pathMatchers(HttpMethod.GET, String.format("/%s/api/v1/patients/**", MicroservicesConstants.PATIENT_SERVICE)).authenticated()
                .pathMatchers(HttpMethod.POST, String.format("/%s/api/v1/patients", MicroservicesConstants.PATIENT_SERVICE)).hasAnyAuthority(Role.NURSE.getAuthority(), Role.ADMIN.getAuthority())
                .pathMatchers(HttpMethod.PUT, String.format("/%s/api/v1/patients/**", MicroservicesConstants.PATIENT_SERVICE)).hasAnyAuthority(Role.NURSE.getAuthority(), Role.ADMIN.getAuthority())

                .pathMatchers(HttpMethod.GET, String.format("/%s/api/v2/patients", MicroservicesConstants.PATIENT_SERVICE)).hasAnyAuthority(Role.DOCTOR.getAuthority(), Role.NURSE.getAuthority(), Role.ADMIN.getAuthority())
                .pathMatchers(HttpMethod.GET, String.format("/%s/api/v2/patients/**", MicroservicesConstants.PATIENT_SERVICE)).authenticated()
                .pathMatchers(HttpMethod.POST, String.format("/%s/api/v2/patients", MicroservicesConstants.PATIENT_SERVICE)).hasAnyAuthority(Role.NURSE.getAuthority(), Role.ADMIN.getAuthority())
                .pathMatchers(HttpMethod.PUT, String.format("/%s/api/v2/patients", MicroservicesConstants.PATIENT_SERVICE)).hasAnyAuthority(Role.NURSE.getAuthority(), Role.ADMIN.getAuthority())

                .pathMatchers(HttpMethod.GET, String.format("/%s/api/v1/doctors", MicroservicesConstants.EMPLOYEE_SERVICE)).permitAll()
                .pathMatchers(HttpMethod.GET, String.format("/%s/api/v1/doctors/**", MicroservicesConstants.EMPLOYEE_SERVICE)).permitAll()
                .pathMatchers(HttpMethod.GET, String.format("/%s/api/v1/nurses", MicroservicesConstants.EMPLOYEE_SERVICE)).permitAll()
                .pathMatchers(HttpMethod.GET, String.format("/%s/api/v1/nurses/**", MicroservicesConstants.EMPLOYEE_SERVICE)).permitAll()

                .pathMatchers(HttpMethod.GET, String.format("/%s/api/v1/procedures", MicroservicesConstants.STAY_SERVICE)).permitAll()
                .pathMatchers(HttpMethod.GET, String.format("/%s/api/v1/procedures/**", MicroservicesConstants.STAY_SERVICE)).permitAll()
                .pathMatchers(HttpMethod.GET, String.format("/%s/api/v1/stays/**", MicroservicesConstants.STAY_SERVICE)).authenticated()
                .pathMatchers(HttpMethod.POST, String.format("/%s/api/v1/stays", MicroservicesConstants.STAY_SERVICE)).hasAnyAuthority(Role.DOCTOR.getAuthority(), Role.NURSE.getAuthority())
                .pathMatchers(HttpMethod.PUT, String.format("/%s/api/v1/stays/**", MicroservicesConstants.STAY_SERVICE)).hasAnyAuthority(Role.NURSE.getAuthority(), Role.DOCTOR.getAuthority())
                .pathMatchers(HttpMethod.GET, String.format("/%s/api/v1/undergoes/**", MicroservicesConstants.STAY_SERVICE)).authenticated()
                .pathMatchers(HttpMethod.POST, String.format("/%s/api/v1/undergoes", MicroservicesConstants.STAY_SERVICE)).hasAnyAuthority(Role.DOCTOR.getAuthority())
                .pathMatchers(HttpMethod.DELETE, String.format("/%s/api/v1/undergoes/**", MicroservicesConstants.STAY_SERVICE)).hasAnyAuthority(Role.DOCTOR.getAuthority(), Role.ADMIN.getAuthority())

                .pathMatchers(HttpMethod.GET, String.format("/%s/api/v1/appointments/**", MicroservicesConstants.APPOINTMENT_SERVICE)).authenticated()
                .pathMatchers(HttpMethod.POST, String.format("/%s/api/v1/appointments", MicroservicesConstants.APPOINTMENT_SERVICE)).hasAnyAuthority(Role.NURSE.getAuthority())
                .pathMatchers(HttpMethod.PUT, String.format("/%s/api/v1/appointments/**", MicroservicesConstants.APPOINTMENT_SERVICE)).hasAnyAuthority(Role.NURSE.getAuthority())
                .pathMatchers(HttpMethod.DELETE, String.format("/%s/api/v1/appointments/**", MicroservicesConstants.APPOINTMENT_SERVICE)).hasAnyAuthority(Role.NURSE.getAuthority(), Role.ADMIN.getAuthority())
                .pathMatchers(HttpMethod.GET, String.format("/%s/api/v1/medications", MicroservicesConstants.APPOINTMENT_SERVICE)).permitAll()
                .pathMatchers(HttpMethod.GET, String.format("/%s/api/v1/medications/**", MicroservicesConstants.APPOINTMENT_SERVICE)).permitAll()
                .pathMatchers(HttpMethod.GET, String.format("/%s/api/v1/prescriptions/**", MicroservicesConstants.APPOINTMENT_SERVICE)).authenticated()
                .pathMatchers(HttpMethod.POST, String.format("/%s/api/v1/prescriptions", MicroservicesConstants.APPOINTMENT_SERVICE)).hasAnyAuthority(Role.DOCTOR.getAuthority(), Role.NURSE.getAuthority())
                .pathMatchers(HttpMethod.DELETE, String.format("/%s/api/v1/prescriptions/**", MicroservicesConstants.APPOINTMENT_SERVICE)).hasAnyAuthority(Role.DOCTOR.getAuthority(), Role.NURSE.getAuthority())

                .pathMatchers("/v2/api-docs", "/swagger-resources", "/swagger-resources/**",
                        "/configuration/ui", "/configuration/security", "/swagger-ui.html", "/webjars/**",
                        "/v3/api-docs/**", "/swagger-ui/**").permitAll()

                .pathMatchers(String.format("/%s/v3/api-docs", MicroservicesConstants.HOSPITAL_MANAGEMENT_SERVICE),
                        String.format("/%s/v3/api-docs", MicroservicesConstants.PATIENT_SERVICE),
                        String.format("/%s/v3/api-docs", MicroservicesConstants.EMPLOYEE_SERVICE),
                        String.format("/%s/v3/api-docs", MicroservicesConstants.USER_MANAGEMENT_SERVICE),
                        String.format("/%s/v3/api-docs", MicroservicesConstants.APPOINTMENT_SERVICE),
                        String.format("/%s/v3/api-docs", MicroservicesConstants.STAY_SERVICE)).permitAll()

                .anyExchange().hasAuthority(Role.ADMIN.getAuthority())

                .and()

                .httpBasic().disable()
                .formLogin().disable()
                .csrf().disable()
                .logout().disable()

                .addFilterAt(new JwtTokenFilter(jwtTokenProvider), SecurityWebFiltersOrder.HTTP_BASIC)

                .build();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration corsConfig = new CorsConfiguration();
        corsConfig.setAllowedOrigins(List.of(frontEndDomain));
        corsConfig.setMaxAge(3600L);
        corsConfig.addAllowedMethod("*");
        corsConfig.addAllowedHeader("*");

        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", corsConfig);
        return source;
    }
}
