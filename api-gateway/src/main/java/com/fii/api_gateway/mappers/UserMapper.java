package com.fii.api_gateway.mappers;

import com.fii.api_gateway.dtos.UserDto;
import com.fii.api_gateway.entities.Role;
import com.fii.api_gateway.entities.User;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public final class UserMapper {
    public User toUser(UserDto userDto) {
        User user = new User();
        user.setEmail(userDto.getEmail());
        user.setPassword(userDto.getPassword());
        user.setOwnerId(userDto.getOwnerId());
        user.setRole(Role.valueOf(userDto.getRole()));

        return user;
    }
}
