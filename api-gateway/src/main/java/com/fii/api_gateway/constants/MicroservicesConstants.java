package com.fii.api_gateway.constants;

public class MicroservicesConstants {
    public static final String HOSPITAL_MANAGEMENT_SERVICE = "hospital-management-service";
    public static final String USER_MANAGEMENT_SERVICE = "user-management-service";
    public static final String PATIENT_SERVICE = "patient-service";
    public static final String EMPLOYEE_SERVICE = "employee-service";
    public static final String STAY_SERVICE = "stay-service";
    public static final String APPOINTMENT_SERVICE = "appointment-service";

    private MicroservicesConstants() {
    }
}
