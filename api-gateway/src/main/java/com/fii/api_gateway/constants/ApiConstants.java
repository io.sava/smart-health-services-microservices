package com.fii.api_gateway.constants;

public class ApiConstants {
    public static final String USERS_MANAGEMENT_SERVICE_URL = "lb://user-management-service/api/v1/users/";

    private ApiConstants() {
    }
}
