package com.fii.api_gateway.constants;

public class ExceptionsConstants {
    public static final String INVALID_TOKEN_MESSAGE = "Expired or invalid JWT token";

    private ExceptionsConstants() {
    }
}
