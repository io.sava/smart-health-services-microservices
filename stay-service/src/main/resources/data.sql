-- procedures
insert into procedures(name, cost)
values ('Consultatie de specialitate medicina interna', 250);
insert into procedures(name, cost)
values ('Ecografie abdominala generala', 300);

insert into procedures(name, cost)
values ('Apendicectomie clasica', 4000);
insert into procedures(name, cost)
values ('Abces hepatic – drenaj', 4000);

insert into procedures(name, cost)
values ('Osteotomia şoldului', 300);
insert into procedures(name, cost)
values ('Artroscopia umărului', 500);

insert into procedures(name, cost)
values ('Masurare tensiune intraoculara', 100);
insert into procedures(name, cost)
values ('Gonioscopie', 150);

insert into procedures(name, cost)
values ('Fibroscopie nazală', 100);
insert into procedures(name, cost)
values ('Exsudat faringian', 150);

insert into procedures(name, cost)
values ('Dermatoscopie leziuni pigmentare', 100);
insert into procedures(name, cost)
values ('Electrocauterizare', 50);

-- patients
insert into patients(id, name)
values (1, 'Sava Ioan');
insert into patients(id, name)
values (2, 'Sava Ionescu');

-- rooms
insert into rooms(hospital_id)
values (1);
insert into rooms(hospital_id)
values (1);
insert into rooms(hospital_id)
values (1);

insert into rooms(hospital_id)
values (2);
insert into rooms(hospital_id)
values (2);
insert into rooms(hospital_id)
values (2);

insert into rooms(hospital_id)
values (3);
insert into rooms(hospital_id)
values (3);
insert into rooms(hospital_id)
values (3);

-- departments
insert into departments(hospital_id)
values (1);
insert into departments(hospital_id)
values (1);
insert into departments(hospital_id)
values (1);
insert into departments(hospital_id)
values (1);
insert into departments(hospital_id)
values (1);
insert into departments(hospital_id)
values (1);

insert into departments(hospital_id)
values (2);
insert into departments(hospital_id)
values (2);
insert into departments(hospital_id)
values (2);
insert into departments(hospital_id)
values (2);
insert into departments(hospital_id)
values (2);
insert into departments(hospital_id)
values (2);

insert into departments(hospital_id)
values (3);
insert into departments(hospital_id)
values (3);
insert into departments(hospital_id)
values (3);
insert into departments(hospital_id)
values (3);
insert into departments(hospital_id)
values (3);
insert into departments(hospital_id)
values (3);

-- nurses
insert into nurses(department_id)
values (1);
insert into nurses(department_id)
values (3);
insert into nurses(department_id)
values (7);
insert into nurses(department_id)
values (10);
insert into nurses(department_id)
values (13);
insert into nurses(department_id)
values (14);
insert into nurses(department_id)
values (16);

-- doctors
insert into doctors(department_id)
values (1);
insert into doctors(department_id)
values (2);
insert into doctors(department_id)
values (3);
insert into doctors(department_id)
values (2);
insert into doctors(department_id)
values (1);
insert into doctors(department_id)
values (4);
insert into doctors(department_id)
values (5);
insert into doctors(department_id)
values (3);
insert into doctors(department_id)
values (6);

insert into doctors(department_id)
values (7);
insert into doctors(department_id)
values (7);
insert into doctors(department_id)
values (8);
insert into doctors(department_id)
values (9);
insert into doctors(department_id)
values (8);
insert into doctors(department_id)
values (7);
insert into doctors(department_id)
values (10);
insert into doctors(department_id)
values (11);
insert into doctors(department_id)
values (9);
insert into doctors(department_id)
values (12);

insert into doctors(department_id)
values (13);
insert into doctors(department_id)
values (13);
insert into doctors(department_id)
values (14);
insert into doctors(department_id)
values (15);
insert into doctors(department_id)
values (14);
insert into doctors(department_id)
values (13);
insert into doctors(department_id)
values (16);
insert into doctors(department_id)
values (17);
insert into doctors(department_id)
values (15);
insert into doctors(department_id)
values (18);

-- stays
insert into stays(patient_id, room_id, start_date, end_date)
values (1, 1, '2020-08-20', '2020-08-27');
insert into stays(patient_id, room_id, start_date, end_date)
values (1, 2, '2020-09-20', '2020-09-27');
insert into stays(patient_id, room_id, start_date, end_date)
values (2, 1, '2020-08-20', '2020-08-27');
insert into stays(patient_id, room_id, start_date, end_date)
values (2, 2, '2020-09-20', '2020-09-27');

-- undergoes
insert into undergoes(doctor_id, procedure_id, stay_id)
values (1, 2, 1);
insert into undergoes(doctor_id, procedure_id, stay_id)
values (1, 2, 2);
insert into undergoes(doctor_id, procedure_id, stay_id)
values (1, 2, 3);
insert into undergoes(doctor_id, procedure_id, stay_id)
values (1, 2, 4);
