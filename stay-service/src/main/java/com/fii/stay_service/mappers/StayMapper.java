package com.fii.stay_service.mappers;

import com.fii.stay_service.dtos.StayDto;
import com.fii.stay_service.entities.Stay;
import com.fii.stay_service.services.PatientService;
import com.fii.stay_service.services.RoomService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public final class StayMapper {
    private final PatientService patientService;
    private final RoomService roomService;

    public Stay toStay(StayDto stayDto) {
        Stay stay = new Stay();
        stay.setStartDate(stayDto.getStartDate());
        stay.setEndDate(stayDto.getEndDate());
        stay.setPatient(patientService.getById(stayDto.getPatientId()));
        stay.setRoom(roomService.getById(stayDto.getRoomId()));

        return stay;
    }
}
