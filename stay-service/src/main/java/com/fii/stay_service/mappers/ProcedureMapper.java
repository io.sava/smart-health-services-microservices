package com.fii.stay_service.mappers;

import com.fii.stay_service.dtos.ProcedureDto;
import com.fii.stay_service.entities.Procedure;
import org.springframework.stereotype.Component;

@Component
public final class ProcedureMapper {
    public Procedure toProcedure(ProcedureDto procedureDto) {
        Procedure procedure = new Procedure();
        procedure.setName(procedureDto.getName());
        procedure.setCost(procedureDto.getCost());

        return procedure;
    }
}
