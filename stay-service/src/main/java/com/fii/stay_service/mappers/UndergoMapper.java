package com.fii.stay_service.mappers;

import com.fii.stay_service.dtos.UndergoDto;
import com.fii.stay_service.entities.Undergo;
import com.fii.stay_service.services.DoctorService;
import com.fii.stay_service.services.ProcedureService;
import com.fii.stay_service.services.StayService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public final class UndergoMapper {
    private final DoctorService doctorService;
    private final StayService stayService;
    private final ProcedureService procedureService;

    public Undergo toUndergo(UndergoDto undergoDto) {
        Undergo undergo = new Undergo();
        undergo.setDoctor(doctorService.getById(undergoDto.getDoctorId()));
        undergo.setStay(stayService.getById(undergoDto.getStayId()));
        undergo.setProcedure(procedureService.getById(undergoDto.getProcedureId()));

        return undergo;
    }
}
