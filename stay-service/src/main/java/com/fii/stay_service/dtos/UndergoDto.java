package com.fii.stay_service.dtos;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class UndergoDto {
    @NotNull(message = "ProcedureId cannot be null")
    private Long procedureId;

    @NotNull(message = "StayId cannot be null")
    private Long stayId;

    @NotNull(message = "DoctorId cannot be null")
    private Long doctorId;
}
