package com.fii.stay_service.dtos;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
public class StayDto {
    @NotNull(message = "Start date cannot be null")
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm")
    private LocalDateTime startDate;

    @NotNull(message = "End date cannot be null")
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm")
    private LocalDateTime endDate;

    @NotNull(message = "PatientId cannot be null")
    private Long patientId;

    @NotNull(message = "RoomId cannot be null")
    private Long roomId;
}
