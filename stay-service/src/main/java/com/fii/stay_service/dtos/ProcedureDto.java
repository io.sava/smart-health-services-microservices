package com.fii.stay_service.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProcedureDto {
    @NotNull(message = "Name cannot be null")
    private String name;

    @NotNull(message = "Cost cannot be null")
    private Float cost;
}
