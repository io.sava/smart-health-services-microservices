package com.fii.stay_service.services.rabbitmq;

import com.fii.stay_service.entities.Room;
import com.fii.stay_service.services.RoomService;
import lombok.AllArgsConstructor;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.annotation.RabbitListenerConfigurer;
import org.springframework.amqp.rabbit.listener.RabbitListenerEndpointRegistrar;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class RoomRabbitMQReceiver implements RabbitListenerConfigurer {
    private final RoomService roomService;

    @Override
    public void configureRabbitListeners(RabbitListenerEndpointRegistrar rabbitListenerEndpointRegistrar) {

    }

    @RabbitListener(queues = "${rabbitmq.queue.create-room}")
    public void receiveCreateRoomMessage(Room room) {
        roomService.create(room);
    }

    @RabbitListener(queues = "${rabbitmq.queue.update-room}")
    public void receiveUpdateRoomMessage(Room room) {
        roomService.update(room);
    }

    @RabbitListener(queues = "${rabbitmq.queue.remove-room}")
    public void receiveRemoveRoomMessage(Long roomId) {
        roomService.remove(roomId);
    }
}
