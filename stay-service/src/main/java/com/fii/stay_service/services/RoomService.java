package com.fii.stay_service.services;

import com.fii.stay_service.entities.Room;
import com.fii.stay_service.exceptions.EntityNotFoundException;
import com.fii.stay_service.repositories.RoomRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class RoomService {
    private final RoomRepository roomRepository;

    public Room getById(Long id) {
        return roomRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Room", id));
    }

    public void create(Room room) {
        roomRepository.save(room);
    }

    public void update(Room room) {
        roomRepository.save(room);
    }

    public void remove(Long roomId) {
        roomRepository.deleteById(roomId);
    }
}
