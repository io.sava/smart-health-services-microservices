package com.fii.stay_service.services;

import com.fii.stay_service.dtos.StayDto;
import com.fii.stay_service.entities.Doctor;
import com.fii.stay_service.entities.Nurse;
import com.fii.stay_service.entities.Role;
import com.fii.stay_service.entities.Room;
import com.fii.stay_service.entities.Stay;
import com.fii.stay_service.entities.Undergo;
import com.fii.stay_service.entities.User;
import com.fii.stay_service.exceptions.EntityNotFoundException;
import com.fii.stay_service.exceptions.InvalidStayDatesException;
import com.fii.stay_service.mappers.StayMapper;
import com.fii.stay_service.repositories.StayRepository;
import com.fii.stay_service.repositories.UndergoRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
@RequiredArgsConstructor
public class StayService {
    private final StayRepository stayRepository;
    private final UndergoRepository undergoRepository;
    private final StayMapper stayMapper;
    private final PatientService patientService;
    private final RoomService roomService;
    private final DoctorService doctorService;
    private final NurseService nurseService;

    public Stay getById(Long id) {
        return stayRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Stay", id));
    }

    public List<Stay> getByPatientId(Long patientId) {
        patientService.checkIfExists(patientId);

        return stayRepository.findAll().stream()
                .filter(stay -> stay.getPatient().getId().equals(patientId))
                .collect(Collectors.toList());
    }

    public void create(StayDto stayDto) {
        if (!checkIfStayDatesAreValid(stayDto.getStartDate(), stayDto.getEndDate())) {
            throw new InvalidStayDatesException();
        }
        stayRepository.save(stayMapper.toStay(stayDto));
    }

    public void update(Long id, StayDto stayDto) {
        this.getById(id);
        if (!checkIfStayDatesAreValid(stayDto.getStartDate(), stayDto.getEndDate())) {
            throw new InvalidStayDatesException();
        }
        Stay stayToUpdate = stayMapper.toStay(stayDto);
        stayToUpdate.setId(id);
        stayRepository.save(stayToUpdate);
    }

    public void remove(Long id) {
        Stay stay = this.getById(id);
        for (Undergo undergo : stay.getUndergoes()) {
            undergoRepository.delete(undergo);
        }
        stayRepository.delete(stay);
    }

    public boolean canUserGetStay(User user, Long stayId) {
        if (user.getRole().equals(Role.PATIENT)) {
            Stay stay = this.getById(stayId);
            return user.getOwnerId().equals(stay.getPatient().getId());
        }
        return true;
    }

    public boolean canUserCreateStay(User user, StayDto stayDto) {
        Room room = this.roomService.getById(stayDto.getRoomId());
        if (user.getRole().equals(Role.DOCTOR)) {
            Doctor doctor = this.doctorService.getById(user.getOwnerId());
            return doctor.getDepartment().getHospitalId().equals(room.getHospitalId());
        }

        if (user.getRole().equals(Role.NURSE)) {
            Nurse nurse = this.nurseService.getById(user.getOwnerId());
            return nurse.getDepartment().getHospitalId().equals(room.getHospitalId());
        }

        return false;
    }

    public boolean canUserUpdateStay(User user, Long id, StayDto stayDto) {
        if (canUserCreateStay(user, stayDto)) {
            Stay stay = getById(id);

            if (user.getRole().equals(Role.DOCTOR)) {
                Doctor doctor = this.doctorService.getById(user.getOwnerId());
                return doctor.getDepartment().getHospitalId().equals(stay.getRoom().getHospitalId());
            }

            if (user.getRole().equals(Role.NURSE)) {
                Nurse nurse = this.nurseService.getById(user.getOwnerId());
                return nurse.getDepartment().getHospitalId().equals(stay.getRoom().getHospitalId());
            }
        }

        return false;
    }

    private boolean checkIfStayDatesAreValid(LocalDateTime startDate, LocalDateTime endDate) {
        return endDate.isAfter(startDate);
    }
}
