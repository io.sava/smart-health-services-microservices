package com.fii.stay_service.services;

import com.fii.stay_service.dtos.UndergoDto;
import com.fii.stay_service.entities.Doctor;
import com.fii.stay_service.entities.Role;
import com.fii.stay_service.entities.Stay;
import com.fii.stay_service.entities.Undergo;
import com.fii.stay_service.entities.User;
import com.fii.stay_service.exceptions.EntityNotFoundException;
import com.fii.stay_service.mappers.UndergoMapper;
import com.fii.stay_service.repositories.UndergoRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
@RequiredArgsConstructor
public class UndergoService {
    private final UndergoRepository undergoRepository;
    private final UndergoMapper undergoMapper;
    private final StayService stayService;
    private final DoctorService doctorService;

    public Undergo getById(Long id) {
        return undergoRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Undergo", id));
    }

    public List<Undergo> getByStayId(Long stayId) {
        stayService.getById(stayId);

        return undergoRepository.findAll().stream()
                .filter(undergo -> undergo.getStay().getId().equals(stayId))
                .collect(Collectors.toList());
    }

    public Page<Undergo> getByDoctorId(Long doctorId, Pageable pageable) {
        doctorService.checkIfExists(doctorId);
        return undergoRepository.findByDoctorId(doctorId, pageable);
    }

    public void create(UndergoDto undergoDto) {
        undergoRepository.save(undergoMapper.toUndergo(undergoDto));
    }

    public void remove(Long id) {
        Undergo undergo = this.getById(id);
        undergoRepository.delete(undergo);
    }

    public boolean canUserCreateUndergo(User user, UndergoDto undergoDto) {
        if (user.getRole().equals(Role.DOCTOR)) {
            Stay stay = stayService.getById(undergoDto.getStayId());
            Doctor doctor = this.doctorService.getById(user.getOwnerId());
            if (doctor.getDepartment().getHospitalId().equals(stay.getRoom().getHospitalId())) {
                return undergoDto.getDoctorId().equals(user.getOwnerId());
            }
        }

        return false;
    }

    public boolean canUserDeleteUndergo(User user, Long undergoId) {
        if (user.getRole().equals(Role.ADMIN)) {
            return true;
        }

        if (user.getRole().equals(Role.DOCTOR)) {
            Undergo undergo = getById(undergoId);
            return undergo.getDoctor().getId().equals(user.getOwnerId());
        }

        return false;
    }
}
