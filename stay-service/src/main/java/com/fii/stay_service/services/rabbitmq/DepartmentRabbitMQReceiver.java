package com.fii.stay_service.services.rabbitmq;

import com.fii.stay_service.entities.Department;
import com.fii.stay_service.services.DepartmentService;
import lombok.AllArgsConstructor;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.annotation.RabbitListenerConfigurer;
import org.springframework.amqp.rabbit.listener.RabbitListenerEndpointRegistrar;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class DepartmentRabbitMQReceiver implements RabbitListenerConfigurer {
    private final DepartmentService departmentService;

    @Override
    public void configureRabbitListeners(RabbitListenerEndpointRegistrar rabbitListenerEndpointRegistrar) {

    }

    @RabbitListener(queues = "${rabbitmq.queue.create-department}")
    public void receiveCreateDepartmentMessage(Department department) {
        departmentService.create(department);
    }

    @RabbitListener(queues = "${rabbitmq.queue.update-department}")
    public void receiveUpdateDepartmentMessage(Department department) {
        departmentService.update(department);
    }

    @RabbitListener(queues = "${rabbitmq.queue.remove-department}")
    public void receiveRemoveDepartmentMessage(Long departmentId) {
        departmentService.remove(departmentId);
    }
}
