package com.fii.stay_service.services.rabbitmq;

import com.fii.stay_service.entities.Doctor;
import com.fii.stay_service.services.DoctorService;
import lombok.AllArgsConstructor;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.annotation.RabbitListenerConfigurer;
import org.springframework.amqp.rabbit.listener.RabbitListenerEndpointRegistrar;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class DoctorRabbitMQReceiver implements RabbitListenerConfigurer {
    private final DoctorService doctorService;

    @Override
    public void configureRabbitListeners(RabbitListenerEndpointRegistrar rabbitListenerEndpointRegistrar) {

    }

    @RabbitListener(queues = "${rabbitmq.queue.create-doctor}")
    public void receiveCreateDoctorMessage(Doctor doctor) {
        doctorService.create(doctor);
    }

    @RabbitListener(queues = "${rabbitmq.queue.update-doctor}")
    public void receiveUpdateDoctorMessage(Doctor doctor) {
        doctorService.update(doctor);
    }

    @RabbitListener(queues = "${rabbitmq.queue.remove-doctor}")
    public void receiveRemoveDoctorMessage(Long doctorId) {
        doctorService.remove(doctorId);
    }
}
