package com.fii.stay_service.services;

import com.fii.stay_service.entities.Patient;
import com.fii.stay_service.exceptions.EntityNotFoundException;
import com.fii.stay_service.repositories.PatientRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PatientService {
    private static final String PATIENT_ENTITY = "Patient";
    private final PatientRepository patientRepository;

    public Patient getById(Long id) {
        return patientRepository.findById(id).orElseThrow(() -> new EntityNotFoundException(PATIENT_ENTITY, id));
    }

    public void checkIfExists(Long id) {
        if (patientRepository.findById(id).isEmpty()) {
            throw new EntityNotFoundException(PATIENT_ENTITY, id);
        }
    }

    public void create(Patient patient) {
        patientRepository.save(patient);
    }

    public void update(Patient patient) {
        patientRepository.save(patient);
    }

    public void remove(Long patientId) {
        patientRepository.deleteById(patientId);
    }
}
