package com.fii.stay_service.exceptions;

public class InvalidStayDatesException extends RuntimeException {
    public InvalidStayDatesException() {
        super("Invalid dates. End date should be greater than start date");
    }
}
