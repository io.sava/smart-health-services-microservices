package com.fii.stay_service.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

@EqualsAndHashCode(callSuper = false)
@Data
@Entity
@Table(name = "rooms")
public class Room extends BaseEntity {
    @Column(name = "hospital_id")
    private Long hospitalId;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "room")
    private List<Stay> stays;

    public Room() {
        this.stays = new ArrayList<>();
    }
}
