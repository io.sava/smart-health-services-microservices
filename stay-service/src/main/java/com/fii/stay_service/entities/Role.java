package com.fii.stay_service.entities;

import org.springframework.security.core.GrantedAuthority;

public enum Role implements GrantedAuthority {
    ADMIN, DOCTOR, PATIENT, NURSE;

    public String getAuthority() {
        return name();
    }
}
