package com.fii.stay_service.entities;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@EqualsAndHashCode(callSuper = false)
@Data
@Entity
@Table(name = "doctors")
public class Doctor extends BaseEntity {
    @ManyToOne
    @JoinColumn(name = "department_id")
    private Department department;
}
