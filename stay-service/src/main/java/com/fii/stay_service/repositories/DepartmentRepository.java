package com.fii.stay_service.repositories;

import com.fii.stay_service.entities.Department;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DepartmentRepository extends JpaRepository<Department, Long> {
}
