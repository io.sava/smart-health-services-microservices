package com.fii.stay_service.repositories;

import com.fii.stay_service.entities.Room;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoomRepository extends JpaRepository<Room, Long> {
}
