package com.fii.stay_service.repositories;

import com.fii.stay_service.entities.Stay;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StayRepository extends JpaRepository<Stay, Long> {
}
