package com.fii.stay_service.repositories;

import com.fii.stay_service.entities.Patient;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PatientRepository extends JpaRepository<Patient, Long> {
}
