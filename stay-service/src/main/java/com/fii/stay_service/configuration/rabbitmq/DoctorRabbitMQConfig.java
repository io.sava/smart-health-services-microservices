package com.fii.stay_service.configuration.rabbitmq;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.core.ExchangeBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DoctorRabbitMQConfig {
    @Value("${spring.rabbitmq.template.doctor-exchange}")
    private String doctorExchange;

    @Value("${rabbitmq.queue.create-doctor}")
    private String createDoctorQueue;

    @Value("${rabbitmq.routing-key.create-doctor}")
    private String createDoctorRoutingKey;

    @Value("${rabbitmq.queue.update-doctor}")
    private String updateDoctorQueue;

    @Value("${rabbitmq.routing-key.update-doctor}")
    private String updateDoctorRoutingKey;

    @Value("${rabbitmq.queue.remove-doctor}")
    private String removeDoctorQueue;

    @Value("${rabbitmq.routing-key.remove-doctor}")
    private String removeDoctorRoutingKey;

    @Bean
    public Exchange getDoctorExchange() {
        return ExchangeBuilder.directExchange(doctorExchange).durable(true).build();
    }

    @Bean
    public Queue createDoctorQueue() {
        return new Queue(createDoctorQueue, true);
    }

    @Bean
    public Binding createDoctorBinding() {
        return BindingBuilder
                .bind(createDoctorQueue())
                .to(getDoctorExchange())
                .with(createDoctorRoutingKey)
                .noargs();
    }

    @Bean
    public Queue updateDoctorQueue() {
        return new Queue(updateDoctorQueue, true);
    }

    @Bean
    public Binding updateDoctorBinding() {
        return BindingBuilder
                .bind(updateDoctorQueue())
                .to(getDoctorExchange())
                .with(updateDoctorRoutingKey)
                .noargs();
    }

    @Bean
    public Queue removeDoctorQueue() {
        return new Queue(removeDoctorQueue, true);
    }

    @Bean
    public Binding removeDoctorBinding() {
        return BindingBuilder
                .bind(removeDoctorQueue())
                .to(getDoctorExchange())
                .with(removeDoctorRoutingKey)
                .noargs();
    }
}
