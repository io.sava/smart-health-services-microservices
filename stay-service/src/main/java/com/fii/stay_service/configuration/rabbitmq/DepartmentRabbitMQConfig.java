package com.fii.stay_service.configuration.rabbitmq;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.core.ExchangeBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DepartmentRabbitMQConfig {
    @Value("${spring.rabbitmq.template.department-exchange}")
    private String departmentExchange;

    @Value("${rabbitmq.queue.create-department}")
    private String createDepartmentQueue;

    @Value("${rabbitmq.routing-key.create-department}")
    private String createDepartmentRoutingKey;

    @Value("${rabbitmq.queue.update-department}")
    private String updateDepartmentQueue;

    @Value("${rabbitmq.routing-key.update-department}")
    private String updateDepartmentRoutingKey;

    @Value("${rabbitmq.queue.remove-department}")
    private String removeDepartmentQueue;

    @Value("${rabbitmq.routing-key.remove-department}")
    private String removeDepartmentRoutingKey;

    @Bean
    public Exchange getDepartmentExchange() {
        return ExchangeBuilder.directExchange(departmentExchange).durable(true).build();
    }

    @Bean
    public Queue createDepartmentQueue() {
        return new Queue(createDepartmentQueue, true);
    }

    @Bean
    public Binding createDepartmentBinding() {
        return BindingBuilder
                .bind(createDepartmentQueue())
                .to(getDepartmentExchange())
                .with(createDepartmentRoutingKey)
                .noargs();
    }

    @Bean
    public Queue updateDepartmentQueue() {
        return new Queue(updateDepartmentQueue, true);
    }

    @Bean
    public Binding updateDepartmentBinding() {
        return BindingBuilder
                .bind(updateDepartmentQueue())
                .to(getDepartmentExchange())
                .with(updateDepartmentRoutingKey)
                .noargs();
    }

    @Bean
    public Queue removeDepartmentQueue() {
        return new Queue(removeDepartmentQueue, true);
    }

    @Bean
    public Binding removeDepartmentBinding() {
        return BindingBuilder
                .bind(removeDepartmentQueue())
                .to(getDepartmentExchange())
                .with(removeDepartmentRoutingKey)
                .noargs();
    }
}
