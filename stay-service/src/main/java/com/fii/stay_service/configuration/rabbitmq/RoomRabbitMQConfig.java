package com.fii.stay_service.configuration.rabbitmq;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.core.ExchangeBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RoomRabbitMQConfig {
    @Value("${spring.rabbitmq.template.room-exchange}")
    private String roomExchange;

    @Value("${rabbitmq.queue.create-room}")
    private String createRoomQueue;

    @Value("${rabbitmq.routing-key.create-room}")
    private String createRoomRoutingKey;

    @Value("${rabbitmq.queue.update-room}")
    private String updateRoomQueue;

    @Value("${rabbitmq.routing-key.update-room}")
    private String updateRoomRoutingKey;

    @Value("${rabbitmq.queue.remove-room}")
    private String removeRoomQueue;

    @Value("${rabbitmq.routing-key.remove-room}")
    private String removeRoomRoutingKey;

    @Bean
    public Exchange getRoomExchange() {
        return ExchangeBuilder.directExchange(roomExchange).durable(true).build();
    }

    @Bean
    public Queue createRoomQueue() {
        return new Queue(createRoomQueue, true);
    }

    @Bean
    public Binding createRoomBinding() {
        return BindingBuilder
                .bind(createRoomQueue())
                .to(getRoomExchange())
                .with(createRoomRoutingKey)
                .noargs();
    }

    @Bean
    public Queue updateRoomQueue() {
        return new Queue(updateRoomQueue, true);
    }

    @Bean
    public Binding updateRoomBinding() {
        return BindingBuilder
                .bind(updateRoomQueue())
                .to(getRoomExchange())
                .with(updateRoomRoutingKey)
                .noargs();
    }

    @Bean
    public Queue removeRoomQueue() {
        return new Queue(removeRoomQueue, true);
    }

    @Bean
    public Binding removeRoomBinding() {
        return BindingBuilder
                .bind(removeRoomQueue())
                .to(getRoomExchange())
                .with(removeRoomRoutingKey)
                .noargs();
    }
}
