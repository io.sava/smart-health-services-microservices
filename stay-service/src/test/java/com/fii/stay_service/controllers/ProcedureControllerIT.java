package com.fii.stay_service.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fii.stay_service.dtos.ProcedureDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
@RunWith(SpringRunner.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class ProcedureControllerIT {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper mapper;

    @Test
    public void getProcedures_shouldReturnOk() throws Exception {
        this.mockMvc.perform(get("/api/v1/procedures"))
                .andExpect(status().isOk());
    }

    @Test
    public void getProcedureWithId_1_shouldReturnOk_whenProcedureWithId_1_isInDatabase() throws Exception {
        Long procedureId = 1L;

        this.mockMvc.perform(get("/api/v1/procedures/{id}", procedureId))
                .andExpect(status().isOk());
    }

    @Test
    public void getProcedureWithId_100_shouldReturnNotFound_whenProcedureWithId_100_isNotInDatabase() throws Exception {
        Long procedureId = 100L;

        this.mockMvc.perform(get("/api/v1/procedures/{id}", procedureId))
                .andExpect(status().isNotFound());
    }

    @Test
    public void createProcedure_shouldReturnCreated() throws Exception {
        ProcedureDto procedure =
                new ProcedureDto("Procedure name", 150F);

        this.mockMvc.perform(post("/api/v1/procedures")
                .content(mapper.writeValueAsString(procedure))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    @Test
    public void updateProcedureWithId_1_shouldReturnNoContent_whenProcedureWithId_1_isInDatabase() throws Exception {
        Long procedureId = 1L;
        ProcedureDto procedure =
                new ProcedureDto("Procedure test", 100F);

        this.mockMvc.perform(put("/api/v1/procedures/{id}", procedureId)
                .content(mapper.writeValueAsString(procedure))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }

    @Test
    public void updateProcedureWithId_100_shouldReturnNotFound_whenProcedureWithId_100_isNotInDatabase() throws Exception {
        Long procedureId = 100L;
        ProcedureDto procedure =
                new ProcedureDto("Procedure test", 100F);

        this.mockMvc.perform(put("/api/v1/procedures/{id}", procedureId)
                .content(mapper.writeValueAsString(procedure))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void deleteProcedureWithId_1_shouldReturnNoContent_whenProcedureWithId_1_isInDatabase() throws Exception {
        Long procedureId = 1L;

        this.mockMvc.perform(delete("/api/v1/procedures/{id}", procedureId))
                .andExpect(status().isNoContent());
    }

    @Test
    public void deleteProcedureWithId_100_shouldReturnNotFound_whenProcedureWithId_100_isNotInDatabase() throws Exception {
        Long procedureId = 100L;

        this.mockMvc.perform(delete("/api/v1/procedures/{id}", procedureId))
                .andExpect(status().isNotFound());
    }
}
