insert into users(email, owner_id, password, role)
values ('admin@gmail.com', null, '$2a$10$NcN86PLYIVpljusXZv/jDuy9b8JXDQDNmY2QcrvkHT49PA3riR5na', 'ADMIN');

insert into users(email, owner_id, password, role)
values ('virgil@gmail.com', 1, '$2a$10$2OkJ6SS7J2a.55IZC334ROcpFT365GoKZNDRXopXNev53oHHqQk6K', 'DOCTOR');
insert into users(email, owner_id, password, role)
values ('mirel@gmail.com', 2, '$2a$10$Am5mqIraKglC8p1z6zdtdOfyJBrBEZ8iJGxRfT44R9iBmWD0YDsl6', 'DOCTOR');

insert into users(email, owner_id, password, role)
values ('angela@gmail.com', 1, '$2a$10$Ne1MUbt945493To.2yhkLOIRjmcJZsF7c/Mn76fQV.1HW8QAgbb4i', 'NURSE');

insert into users(email, owner_id, password, role)
values ('ioan.sava@gmail.com', 1, '$2a$10$O8pfw3H3e.KvREtXJ6kreuIf9UxXUH4oPKd56Mi033mYG8th1ckOK', 'PATIENT');
insert into users(email, owner_id, password, role)
values ('io.sava@gmail.com', 2, '$2y$10$6fi/5a8ZV2fybHxuCF.xm.xKB3mMvYI2lTEcHpCAx1AThjqJiG3Ju', 'PATIENT');