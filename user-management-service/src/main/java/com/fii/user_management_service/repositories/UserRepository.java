package com.fii.user_management_service.repositories;

import com.fii.user_management_service.entities.Role;
import com.fii.user_management_service.entities.User;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.Optional;

public interface UserRepository extends CrudRepository<User, Long> {
    boolean existsByEmail(String email);

    Optional<User> findByEmail(String email);

    Optional<User> findByOwnerIdAndRole(Long ownerId, Role role);

    @Transactional
    @Modifying
    @Query("update User u set u.email = :email where u.id = :id")
    void updateEmail(@Param("id") Long id, @Param("email") String email);

    @Transactional
    void deleteByEmail(String email);
}
