package com.fii.user_management_service.configuration;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.core.ExchangeBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQConfig {
    @Value("${spring.rabbitmq.username}")
    private String username;

    @Value("${spring.rabbitmq.password}")
    private String password;

    @Value("${spring.rabbitmq.host}")
    private String host;

    @Value("${spring.rabbitmq.template.user-exchange}")
    private String exchange;

    @Value("${rabbitmq.queue.create-account}")
    private String createAccountQueue;

    @Value("${rabbitmq.routing-key.create-account}")
    private String createAccountRoutingKey;

    @Value("${rabbitmq.queue.update-account-email}")
    private String updateAccountEmailQueue;

    @Value("${rabbitmq.routing-key.update-account-email}")
    private String updateAccountEmailRoutingKey;

    @Value("${rabbitmq.queue.remove-account-by-email}")
    private String removeAccountByEmailQueue;

    @Value("${rabbitmq.routing-key.remove-account-by-email}")
    private String removeAccountByEmailRoutingKey;

    @Bean
    public Exchange getExchange() {
        return ExchangeBuilder.directExchange(exchange).durable(true).build();
    }

    @Bean
    public Queue createAccountQueue() {
        return new Queue(createAccountQueue, true);
    }

    @Bean
    public Binding createAccountBinding() {
        return BindingBuilder
                .bind(createAccountQueue())
                .to(getExchange())
                .with(createAccountRoutingKey)
                .noargs();
    }

    @Bean
    public Queue updateAccountEmailQueue() {
        return new Queue(updateAccountEmailQueue, true);
    }

    @Bean
    public Binding updateAccountEmailBinding() {
        return BindingBuilder
                .bind(updateAccountEmailQueue())
                .to(getExchange())
                .with(updateAccountEmailRoutingKey)
                .noargs();
    }

    @Bean
    public Queue removeAccountByEmailQueue() {
        return new Queue(removeAccountByEmailQueue, true);
    }

    @Bean
    public Binding removeAccountByEmailBinding() {
        return BindingBuilder
                .bind(removeAccountByEmailQueue())
                .to(getExchange())
                .with(removeAccountByEmailRoutingKey)
                .noargs();
    }

    @Bean
    public ConnectionFactory connectionFactory() {
        CachingConnectionFactory cachingConnectionFactory = new CachingConnectionFactory(host);
        cachingConnectionFactory.setUsername(username);
        cachingConnectionFactory.setPassword(password);
        return cachingConnectionFactory;
    }

    @Bean
    public MessageConverter jsonMessageConverter() {
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    public RabbitTemplate rabbitTemplate(ConnectionFactory connectionFactory) {
        final RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(jsonMessageConverter());
        return rabbitTemplate;
    }
}
