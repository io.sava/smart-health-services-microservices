package com.fii.user_management_service.exceptions;

public class InvalidRoleException extends RuntimeException {
    public InvalidRoleException(String role) {
        super(String.format("Invalid role: %s. Available roles: PATIENT, NURSE, DOCTOR, ADMIN", role));
    }
}
