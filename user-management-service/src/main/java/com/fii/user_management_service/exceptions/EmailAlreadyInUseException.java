package com.fii.user_management_service.exceptions;

public class EmailAlreadyInUseException extends RuntimeException {
    public EmailAlreadyInUseException(String email) {
        super(String.format("Email %s is already in use", email));
    }
}
