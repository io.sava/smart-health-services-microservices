package com.fii.user_management_service.dtos;

import lombok.Data;

@Data
public class UpdateAccountEmailDto {
    private Long ownerId;
    private String email;
    private String role;
}
