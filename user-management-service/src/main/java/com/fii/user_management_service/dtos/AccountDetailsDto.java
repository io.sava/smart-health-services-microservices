package com.fii.user_management_service.dtos;

import lombok.Data;

@Data
public class AccountDetailsDto {
    private Long ownerId;
    private String email;
    private String role;
}
