package com.fii.user_management_service.services;

import org.passay.CharacterData;
import org.passay.CharacterRule;
import org.passay.EnglishCharacterData;
import org.passay.PasswordGenerator;
import org.springframework.stereotype.Service;

@Service
public final class PasswordGeneratorService {
    private static final int PASSWORD_LENGTH = 15;
    private static final int NUMBER_OF_CHARACTERS_PER_RULE = 2;

    public String generatePassword() {
        PasswordGenerator passwordGenerator = new PasswordGenerator();

        return passwordGenerator.generatePassword(PASSWORD_LENGTH, getSpecialCharactersRule(), getLowerCaseRule(),
                getUpperCaseRule(), getDigitRule());
    }

    private CharacterRule getLowerCaseRule() {
        CharacterData lowerCaseChars = EnglishCharacterData.LowerCase;
        CharacterRule lowerCaseRule = new CharacterRule(lowerCaseChars);
        lowerCaseRule.setNumberOfCharacters(NUMBER_OF_CHARACTERS_PER_RULE);
        return lowerCaseRule;
    }

    private CharacterRule getUpperCaseRule() {
        CharacterData upperCaseChars = EnglishCharacterData.UpperCase;
        CharacterRule upperCaseRule = new CharacterRule(upperCaseChars);
        upperCaseRule.setNumberOfCharacters(NUMBER_OF_CHARACTERS_PER_RULE);
        return upperCaseRule;
    }

    private CharacterRule getDigitRule() {
        CharacterData digitChars = EnglishCharacterData.Digit;
        CharacterRule digitRule = new CharacterRule(digitChars);
        digitRule.setNumberOfCharacters(NUMBER_OF_CHARACTERS_PER_RULE);
        return digitRule;
    }

    private CharacterRule getSpecialCharactersRule() {
        CharacterData specialCharacters = new CharacterData() {
            @Override
            public String getErrorCode() {
                return null;
            }

            public String getCharacters() {
                return "!@#$%^&*()_+";
            }
        };

        CharacterRule specialCharactersRule = new CharacterRule(specialCharacters);
        specialCharactersRule.setNumberOfCharacters(NUMBER_OF_CHARACTERS_PER_RULE);
        return specialCharactersRule;
    }
}
