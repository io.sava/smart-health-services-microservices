package com.fii.user_management_service.services;

import com.fii.user_management_service.dtos.AccountDetailsDto;
import com.fii.user_management_service.dtos.UpdateAccountEmailDto;
import com.fii.user_management_service.dtos.UserLoginDto;
import com.fii.user_management_service.dtos.UserLoginResultDto;
import com.fii.user_management_service.dtos.UserRegisterDto;
import com.fii.user_management_service.entities.Role;
import com.fii.user_management_service.entities.User;
import com.fii.user_management_service.exceptions.EmailAlreadyInUseException;
import com.fii.user_management_service.exceptions.EntityNotFoundException;
import com.fii.user_management_service.exceptions.InvalidRoleException;
import com.fii.user_management_service.mappers.UserMapper;
import com.fii.user_management_service.repositories.UserRepository;
import com.fii.user_management_service.security.JwtTokenProvider;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;
    private final UserMapper userMapper;
    private final PasswordGeneratorService passwordGeneratorService;
    private final MailSenderService mailSenderService;
    private final JwtTokenProvider jwtTokenProvider;
    private final AuthenticationManager authenticationManager;

    @Value("${account.mail.subject}")
    private String emailSubject;

    @Value("${account.mail.text}")
    private String emailText;

    public User getByOwnerIdAndRole(Long ownerId, Role role) {
        return userRepository.findByOwnerIdAndRole(ownerId, role).orElseThrow(() -> new EntityNotFoundException("User"));
    }

    public User getByEmail(String email) {
        return userRepository.findByEmail(email).orElseThrow(() -> new EntityNotFoundException("User"));
    }

    public void generateAccount(AccountDetailsDto accountDetails) {
        Role role = getRoleByString(accountDetails.getRole());
        String password = passwordGeneratorService.generatePassword();
        UserRegisterDto newUser =
                new UserRegisterDto(accountDetails.getEmail(), password, role.getAuthority(), accountDetails.getOwnerId());
        register(newUser);
        mailSenderService.sendEmail(accountDetails.getEmail(), emailSubject,
                String.format(emailText, accountDetails.getEmail(), password));
    }

    public UserLoginResultDto login(UserLoginDto userLoginDto) {
        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(userLoginDto.getEmail(), userLoginDto.getPassword()));

        User user = getByEmail(userLoginDto.getEmail());
        String token = jwtTokenProvider.createToken(user);
        return new UserLoginResultDto(user.getOwnerId(), user.getRole().toString(), token);
    }

    public void register(UserRegisterDto userRegisterDto) {
        if (userRepository.existsByEmail(userRegisterDto.getEmail())) {
            throw new EmailAlreadyInUseException(userRegisterDto.getEmail());
        }
        User user = userMapper.toUser(userRegisterDto);
        userRepository.save(user);
    }

    public void updateEmail(UpdateAccountEmailDto updateAccountEmailDto) {
        User user = getByOwnerIdAndRole(updateAccountEmailDto.getOwnerId(), getRoleByString(updateAccountEmailDto.getRole()));
        userRepository.updateEmail(user.getId(), updateAccountEmailDto.getEmail());
    }

    public void remove(String email) {
        getByEmail(email);
        userRepository.deleteByEmail(email);
    }

    private Role getRoleByString(String role) {
        try {
            return Role.valueOf(role);
        } catch (IllegalArgumentException exception) {
            throw new InvalidRoleException(role);
        }
    }
}
