package com.fii.user_management_service.services.rabbitmq;

import com.fii.user_management_service.dtos.AccountDetailsDto;
import com.fii.user_management_service.dtos.UpdateAccountEmailDto;
import com.fii.user_management_service.services.UserService;
import lombok.AllArgsConstructor;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.annotation.RabbitListenerConfigurer;
import org.springframework.amqp.rabbit.listener.RabbitListenerEndpointRegistrar;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class RabbitMQReceiver implements RabbitListenerConfigurer {
    private final UserService userService;

    @Override
    public void configureRabbitListeners(RabbitListenerEndpointRegistrar rabbitListenerEndpointRegistrar) {

    }

    @RabbitListener(queues = "${rabbitmq.queue.create-account}")
    public void receiveCreateAccountMessage(AccountDetailsDto accountDetails) {
        userService.generateAccount(accountDetails);
    }

    @RabbitListener(queues = "${rabbitmq.queue.update-account-email}")
    public void receiveUpdateAccountEmailMessage(UpdateAccountEmailDto updateAccountEmailDto) {
        userService.updateEmail(updateAccountEmailDto);
    }

    @RabbitListener(queues = "${rabbitmq.queue.remove-account-by-email}")
    public void receiveRemoveAccountByEmailMessage(String email) {
        userService.remove(email);
    }
}
